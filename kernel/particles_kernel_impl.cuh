/*
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

/*
 * CUDA particle system kernel code.
 */
/*
 * moving sphere triangle collision wrote Igor Kravtchenko  base by Ericsson - real-time collision detection
 * Cloth verlet wrote by Marco Fratarcangeli
 */
#ifndef _PARTICLES_KERNEL_H_
#define _PARTICLES_KERNEL_H_

#include "helper_math.h"
#include "math_constants.h"
#include "particles_kernel.cuh"
#define COLEVEL	0			///// 3 different level of collision <<< 0 for sphere vs plane >>><<< 1 for plane vs sphere + 3 varices vs sphere >>><<< 2 for plane vs sphere +3 varices vs sphere +edge vs sphere >>>
#define ZCURVE 0			///// 0 for linear (better method) 1 for Z Curve
#define SELFCOL 0
#define square(a) ((a)*(a))
#if USE_TEX
// textures for cloth particle position and objects face
texture<float4, 1, cudaReadModeElementType> oldPosTex;
texture<float4, 1, cudaReadModeElementType> faceTex;

texture<uint, 1, cudaReadModeElementType> gridParticleHashTex;
texture<uint, 1, cudaReadModeElementType> cellStartTex;
texture<uint, 1, cudaReadModeElementType> cellEndTex;

#endif

// simulation parameters in constant memory
__constant__ SimParams params;


__device__ int2 nextNeigh(int n)
{
	if (n == 0)		return make_int2(-1, -1);
	if (n == 1)		return make_int2( 0, -1);
	if (n == 2)		return make_int2( 1, -1);
	if (n == 3)		return make_int2( 1,  0);
	if (n == 4)		return make_int2( 1,  1);
	if (n == 5)		return make_int2( 0,  1);
	if (n == 6)		return make_int2(-1,  1);
	if (n == 7)		return make_int2(-1,  0);

	if (n == 8)		return make_int2(-2, -2);
	if (n == 9)		return make_int2( 2, -2);
	if (n == 10)	return make_int2( 2,  2);
	if (n == 11)	return make_int2(-2,  2);

	return make_int2(0, 0);
}
///////////////////////////////////////////////////////////////////////////////
//! kernel for cloth simulating via verlet integration
//! @param g_odata  memory to process (in and out)
///////////////////////////////////////////////////////////////////////////////
// calculate position in uniform grid
__device__ int3 calcGridPos(float3 p)
{

	int3 gridPos;
#if(ZCURVE)

		gridPos.x = min(max(p.x * 64.0f, 0.0f), 63.0f);
		gridPos.y = min(max(p.y * 64.0f, 0.0f), 63.0f);
		gridPos.z = min(max(p.z * 64.0f, 0.0f), 63.0f);

#else

		gridPos.x = floor((p.x - params.worldOrigin.x) / params.cellSize.x);
    	gridPos.y = floor((p.y - params.worldOrigin.y) / params.cellSize.y);
    	gridPos.z = floor((p.z - params.worldOrigin.z) / params.cellSize.z);

#endif
    return gridPos;
}
__device__ uint expand(uint v)
{
    v = (v * 0x00010001u) & 0xFF0000FFu;
    v = (v * 0x00000101u) & 0x0F00F00Fu;
    v = (v * 0x00000011u) & 0xC30C30C3u;
    v = (v * 0x00000005u) & 0x49249249u;
    return v;
}
// calculate address in grid from position (clamping to edges)
__device__ uint calcGridHash(int3 gridPos)
{
#if(!ZCURVE)

    gridPos.x = gridPos.x & (params.gridSize.x-1);  // wrap grid, assumes size is power of 2  (gridpos%gridsize==gridpos &(gridsize-1))since gridsize is power 2
    gridPos.y = gridPos.y & (params.gridSize.y-1);
    gridPos.z = gridPos.z & (params.gridSize.z-1);

    return ((gridPos.z* params.gridSize.y)* params.gridSize.x) + (gridPos.y* params.gridSize.x) + gridPos.x;

#else

		uint xx = expand((uint)gridPos.x);
	    uint yy = expand((uint)gridPos.y);
	    uint zz = expand((uint)gridPos.z);
	    return xx * 4 + yy * 2 + zz;

#endif
}

// calculate grid hash value for each particle
__global__
void calcHashD(uint   *gridParticleHash,  // output
               uint   *gridParticleIndex, // output
               float4 *pos,               // input: positions
               uint    numParticles,
               uint num_obj)
			                                                 											  
{
    uint index = (blockIdx.x* blockDim.x) + threadIdx.x;

    if (index >= numParticles+num_obj) return;
    volatile float4 p = pos[index];

    if(index>=numParticles)
    {

    	 p.x+=params.colliderPos.x;
    	 p.y+=params.colliderPos.y;
    	 p.z+=params.colliderPos.z;
    }

    // get address in grid
    int3 gridPos = calcGridPos(make_float3(p.x, p.y, p.z));
    uint hash = calcGridHash(gridPos);

    // store grid hash and particle index
    gridParticleHash[index] = hash;
    gridParticleIndex[index] = index;
	
		
}

// rearrange particle data into sorted order, and find the start of each cell
// in the sorted hash array
__global__
void reorderDataAndFindCellStartD (uint   *cellStart,        // output: cell start index
                                  uint   *cellEnd,          // output: cell end index
                                  float4 *sortedPos,        // output: sorted positions
                                  uint   *gridParticleHash, // input: sorted grid hashes
                                  uint   *gridParticleIndex,// input: sorted particle indices
                                  float4 *oldPos,           // input: sorted position array

                                  uint    numParticles,
                                  uint obj)
{
    extern __shared__ uint sharedHash[];    // blockSize + 1 elements
    uint index = (blockIdx.x*blockDim.x) + threadIdx.x;

    ///////
	uint hash;

    // handle case when no. of particles not multiple of block size
    if (index < numParticles+obj)
    {
        hash = gridParticleHash[index];

        // Load hash data into shared memory so that we can look
        // at neighboring particle's hash value without loading
        // two hash values per thread
        sharedHash[threadIdx.x+1] = hash;

        if (index > 0 && threadIdx.x == 0)
        {
            // first thread in block must load neighbor particle hash
            sharedHash[0] = gridParticleHash[index-1];
        }
    }

    __syncthreads();

    if (index < numParticles+obj)
    {
        // If this particle has a different cell index to the previous
        // particle then it must be the first particle in the cell,
        // so store the index of this particle in the cell.
        // As it isn't the first particle, it must also be the cell end of
        // the previous particle's cell

        if (index == 0 || hash != sharedHash[threadIdx.x])
        {
            cellStart[hash] = index;

            if (index > 0)
                cellEnd[sharedHash[threadIdx.x]] = index;
        }

        if (index == numParticles+obj - 1)
        {
            cellEnd[hash] = index + 1;
        }

        // Now use the sorted index to reorder the pos data
        uint sortedIndex = gridParticleIndex[index];
        float4 pos = FETCH(oldPos, sortedIndex);       // macro does either global read or texture fetch


        sortedPos[index] = pos;

    }


}
//testIntersectionLineLine
__device__
bool testIntersectionSphereLine(const float3 pos,
								  const float rad,
								  const float3 _pt0,
								  const float3 _pt1,
								  int   *_nbInter,
								  float *_inter1,
								  float *_inter2)
{
	float a, b, c, i;
	a = square(_pt1.x - _pt0.x) + square(_pt1.y - _pt0.y) + square(_pt1.z - _pt0.z);
	b =  2 * ( (_pt1.x - _pt0.x) * (_pt0.x - pos.x)
		+ (_pt1.y - _pt0.y) * (_pt0.y - pos.y)
		+ (_pt1.z - _pt0.z) * (_pt0.z - pos.z) ) ;
	c = square(pos.x) + square(pos.y)   +
		square(pos.z) + square(_pt0.x)  +
		square(_pt0.y) + square(_pt0.z) -
		2 * ( pos.x * _pt0.x + pos.y * _pt0.y + pos.z * _pt0.z ) - square(rad) ;
	i =  b * b - 4 * a * c;

	if (i < 0)
		return 0;

	if (i == 0) {
		if (_nbInter) *_nbInter = 1;
		if (_inter1) *_inter1 = -b / (2 * a);
	}
	else {
		if (_nbInter) *_nbInter = 2;
		if (_inter1) *_inter1 = (-b + sqrtf( square(b) - 4*a*c )) / (2 * a);
		if (_inter2) *_inter2 = (-b - sqrtf( square(b) - 4*a*c )) / (2 * a);
	}

	return 1;
}

__device__
float distancePointToLine(	 const float3 _point,
							 const float3 _pt0,
							 const float3 _pt1,
							 float3 *_linePt)
{
	float3 v = _point - _pt0;
	float3 s = _pt1 - _pt0;
	float lenSq = s.x*s.x+s.y*s.y+s.z*s.z;
	float dotp = dot(v,s) / lenSq;
	float3 disp = s * dotp;
	if (_linePt)
		*_linePt = _pt0 + disp;
	v -= disp;
	return length(v);
}

__device__
bool testIntersectionLineLine(const float2 _p1, const float2 _p2,
								const float2 _p3, const float2 _p4,
								float *_t)
{
	float2 d1 = _p2 - _p1;
	float2 d2 = _p3 - _p4;

	float denom = d2.y*d1.x - d2.x*d1.y;
	if (!denom)
		return 0;

	if (_t) {
		float dist = d2.x*(_p1.y-_p3.y) - d2.y*(_p1.x-_p3.x);
		dist /= denom;
		*_t = dist;
	}

	return 1;
}

__device__
float3 project(const float3 pos,const float3 &_p0, const float3 &_p1, const float3 &_p2)
	{
		float3 v0=(_p0 - _p1);
		float3 v1=(_p2 - _p1);
		float3 n = cross(v1,v0);
		n = normalize(n);
		float h=n.x * pos.x + n.y * pos.y + n.z * pos.z-(_p0.x * n.x + _p0.y * n.y + _p0.z * n.z) ;
		
		return make_float3(pos.x - n.x * h, pos.y - n.y * h, pos.z - n.z * h);
	}

// collide two spheres using DEM method

__device__
bool isPointInsideTriangle(	 const float3 vertex0,
							 const float3 vertex1,
							 const float3 vertex2,
							 const float3 pt)
{
	float3 u = vertex1 - vertex0;
	float3 v = vertex2 - vertex0;
	float3 w = pt - vertex0;

	float uu = dot(u,u);
	float uv = dot(u,v);
	float vv = dot(v,v);
	float wu = dot(w,u);
	float wv = dot(w,v);
	float d = uv * uv - uu * vv;

	float invD = 1 / d;
	float s = (uv * wv - vv * wu) * invD;
	if (s < 0 || s > 1)
		return 0;
	float t = (uv * wu - uu * wv) * invD;
	if (t < 0 || (s + t) > 1)
		return 0;

	return 1;
}
__device__
float3 collideSelf(float3 posA, float3 posB,float3 vel,
							  float radiusA,float radiusB)
{
	float3 relPos = posA - posB;
	float3 force;
	    float dist = length(relPos);
	    float collideDist = radiusB+radiusA;

	    if (dist < collideDist)
	    {

			//Simple Sphere collision detection
			float3 coll_dir = normalize(posA - posB);
			force= posB + coll_dir * radiusB;
	     }
	    else
	    	force=make_float3(0.0);
	    return force;

}
__device__
bool collideSpheres(  float3 posA, float3 posB,
                      float3 velA, float3 velB,
                      float radiusA, float radiusB,
                      float3 p1,float3 p2, float3 p3,
					  float *_distTravel,float3 *_reaction)
{
    // calculate relative position
 
	float3 relPos = posA - posB;

    float dist = length(relPos);
    float collideDist = radiusB;

    if (dist < collideDist)
    {

		//Simple Sphere collision detection 
		/*float3 coll_dir = normalize(posA - posB);
		*_force= posB + coll_dir * radiusB;
    	 return 1;*/
////
////////Triangle-moving ball collision
#if(COLEVEL==0 ||COLEVEL==1 ||COLEVEL==2)
		float3 nvel=velA;
		nvel=normalize(nvel);
		float3 tri1=p2-p1;
		float3 tri2=p3-p1;
		float3 _triNormal=normalize(cross(tri1,tri2));
		if (dot(_triNormal,nvel) > -0.001f)
				return 0;

		int col = -1;
		*_distTravel = FLT_MAX;
		float3 normt=_triNormal;
		normt=normalize(normt);
		float hd =-(p1.x * normt.x + p1.y * normt.y + p1.z * normt.z);
		// pass1: sphere VS plane
	float h = posA.x*normt.x + posA.y * normt.y + posA.z * normt.z+hd;
	if (h < -radiusA)
		return 0;

	if (h > radiusA) 
	{
		h -= radiusA;
		float dotp = dot(_triNormal,nvel);
		if (dotp != 0) 
		{
			float t = -h / dotp;
			float3 onPlane = posA + nvel * t;
			if (isPointInsideTriangle( p1, p2, p3, onPlane)) 
			{
				if (t < *_distTravel) 
				{
					*_distTravel = t;
					if (_reaction)
						*_reaction = _triNormal;
					col = 0;
				}
			}
		}
	}
#if(COLEVEL==1||COLEVEL==2)
	float3 ver[3];
	ver[0]=p1;
	ver[1]=p2;
	ver[2]=p3;
//		 pass2: sphere VS triangle vertices
	for (int i = 0; i < 3; i++) 
	{
		
		float3 seg_pt0 = ver[i];
		float3 seg_pt1 = seg_pt0 - nvel;
		float3 v = seg_pt1 - seg_pt0;

		float inter1=FLT_MAX, inter2=FLT_MAX;
		int nbInter;
		bool res = testIntersectionSphereLine(posA,radiusA, seg_pt0, seg_pt1, &nbInter, &inter1, &inter2);
		if (res==0)
			continue;

		float t = inter1;
		if (inter2 < t)
			t = inter2;

		if (t < 0)
			continue;

		if (t < *_distTravel) {
			*_distTravel = t;
			float3 onSphere = seg_pt0 + v * t;
			if (_reaction)
				*_reaction = posA - onSphere;
			col = 1;
		}
	}
#if(COLEVEL==2)
	// pass3: sphere VS triangle edges
	for (int i = 0; i < 3; i++)
	{
		float3 edge0 = ver[i];
		int j = i + 1;
		if (j == 3)
			j = 0;
		float3 edge1 =ver[j];
		float3 edge3=edge0-edge1;
		float3 norm=cross(edge3,nvel);
		norm=normalize(norm);
		
		float d = norm.x * posA.x + norm.y * posA.y + norm.z * posA.z-(edge0.x * norm.x + edge0.y * norm.y + edge0.z * norm.z);
		if (d > radiusA || d < -radiusA)
			continue;

		float srr = radiusA * radiusA;
		float r = sqrtf(srr - d*d);

		float3 pt0=make_float3(posA.x - norm.x * d, posA.y - norm.y * d, posA.z - norm.z * d);
		float3 onLine;
		float h = distancePointToLine(pt0, edge0, edge1, &onLine);
		float3 v = onLine - pt0;
		v=normalize(v);
		float3 pt1 = v * r + pt0; // point on the sphere that will maybe collide with the edge
		int a0 = 0, a1 = 1;
		float pl_x = fabsf(norm.x);
		float pl_y = fabsf(norm.y);
		float pl_z = fabsf(norm.z);
		if (pl_x > pl_y && pl_x > pl_z)
		{
			a0 = 1;
			a1 = 2;
		}
		else
		{
			if (pl_y > pl_z)
			{
				a0 = 0;
				a1 = 2;
			}
		}
		float3 vv = pt1 + nvel;
		float t;
		bool res;
		if(a0==0)
		{
			if(a1==1)
				res = testIntersectionLineLine( make_float2(pt1.x,pt1.y),
												make_float2(vv.x,vv.y),
												make_float2(edge0.x,edge0.y),
												make_float2(edge1.x,edge1.y),
												&t);

			else
				res = testIntersectionLineLine( make_float2(pt1.x,pt1.z),
												make_float2(vv.x,vv.z),
												make_float2(edge0.x,edge0.z),
												make_float2(edge1.x,edge1.z),
												&t);
		}
		else
				res = testIntersectionLineLine(  make_float2(pt1.y,pt1.z),
											 make_float2(vv.y,vv.z),
											 make_float2(edge0.y,edge0.z),
											 make_float2(edge1.y,edge1.z),
											 &t);

		if (!res || t < 0)
			continue;

		float3 inter = pt1 + nvel * t;

		float3 r1 = edge0 - inter;
		float3 r2 = edge1 - inter;
		if (dot(r1,r2) > 0)
			continue;

		if (t > *_distTravel)
			continue;

		*_distTravel = t;
		if (_reaction)
			*_reaction = posA - pt1;
		col = 2;
	}
#endif
#endif
#endif
	if (_reaction && col != -1)
		*_reaction=normalize(*_reaction);
		
	 return col == -1 ? 0 : 1;
    }
	else
		return 0;
}

__device__
bool collideSelf(float3 posA, float3 posB,
							float3 velA,float3 velB,
							float radiusA,float radiusB,
							float3 * _force)
{
     	float3 relPos = posB - posA;
	    float dist = length(relPos);
	    float collideDist = radiusA + radiusB;

	    if (dist < collideDist)
	    {
	        float3 norm = relPos / dist;

	        // relative velocity
	        float3 relVel = velB - velA;

	        // relative tangential velocity
	        float3 tanVel = relVel - (dot(relVel, norm) * norm);
	        // spring force
	        *_force = -params.spring*(collideDist - dist) * norm;
	        // dashpot (damping) force
	        *_force += params.damping*relVel;
	        // tangential shear force
	        *_force += params.shear*tanVel;
	        // attraction
	        *_force += params.attraction*relPos;
	        return 1;
	    }
	    return 0;

}

// collide a particle against all other particles in a given cell
__device__
bool collideCell(  int3    gridPos,
                   uint    index,
                   float3  pos,
                   float3  vel,
                   float4 *oldPos,
                   uint   *cellStart,
                   uint   *cellEnd,
				   uint    side,
				   uint    objs,
				   float4 *face,
				   uint   *gridParticleIndex,
				   float3 *posAlt,
				   float   *_distTravel,
				   float4 *oldpost)
{
	uint numParticle =side*side;
    uint gridHash = calcGridHash(gridPos);
	//float3 pos1 =make_float3(pos);
    // get start of bucket for this cell
    uint startIndex = FETCH(cellStart, gridHash);

    float3 force=make_float3(0.0f);
	float distTravel,dis=1000;
	*posAlt=make_float3(0.0);
	float3 posT=make_float3(0.0);
	float3 selfForce=make_float3(0.0f);
	float3 pos_self=make_float3(0.0f);
	bool intersec=0,t=0,t1=0,t2=0;
	float3 self=make_float3(0.0f);
	uint count=0,count1=0;
    if (startIndex != 0xffffffff )          // cell is not empty
    {
		// iterate over particles in this cell
        uint endIndex = FETCH(cellEnd, gridHash);
		
        for (uint j=startIndex; j<endIndex; j++)
        {
             uint originalIndex = gridParticleIndex[j];
				 if (originalIndex >= numParticle)            // check not colliding with self and do not check colide with our object
            {


				float3 p1= make_float3(FETCH(face,(originalIndex-numParticle)*3))  +params.colliderPos;
				float3 p2= make_float3(FETCH(face,(originalIndex-numParticle)*3+1))+params.colliderPos;
				float3 p3= make_float3(FETCH(face,(originalIndex-numParticle)*3+2))+params.colliderPos;
//				float3 p1= make_float3(face[(originalIndex-numParticle)*3] ) +params.colliderPos;
//				float3 p2= make_float3(face[(originalIndex-numParticle)*3+1])+params.colliderPos;
//				float3 p3= make_float3(face[(originalIndex-numParticle)*3+2])+params.colliderPos;

				float3 pos2 = make_float3(FETCH(oldPos, j))+params.colliderPos;
                // collide two spheres  (0.01,0.001)(0.2,0.1)
				intersec= collideSpheres(pos, pos2, vel,params.colliderPos/0.01, 0.1f, FETCH(oldPos, j).w,p1,p2,p3,&distTravel,&force);
				if(intersec && distTravel<0.2 )
				{
					count++;
					intersec=0;
					t=1;
					posT+=force*0.01;
					force=make_float3(0.0f);
				}

			}
				 //Self Colision
#if(SELFCOL)
                 else if(originalIndex<numParticle && j != index &&
                                         j !=(index / side -1) * side + index % side -1 && j !=(index / side) * side + index % side -1 && j !=(index / side) * side + index % side + -1
                                         && j !=(index / side + 1) * side + index % side  && j !=(index / side + 1) * side + index % side + 1 && j !=(index / side + 0) * side + index % side + 1
                                         && j !=(index / side -1) * side + index % side + 1 && j !=(index / side -1) * side + index % side )////self collision
                                {

                                     float3 pos2 = make_float3(FETCH(oldPos, j));
                                     t1=collideSelf(pos,pos2,vel,(pos2-make_float3(oldpost[originalIndex]))/0.01,0.01 ,0.01,&self);
                                     if(t1)
                                     {
                                         pos_self+=self*0.01;
                                         count1++;
                                         t1=0;
                                         t2=1;
                                         self=make_float3(0.0f);
                                     }
                                }
#endif
        }

    }
    if(t && t2)
   	{
   		*posAlt=pos+posT/count+pos_self/count1;
   		*_distTravel=dis;
   		posT=make_float3(0.0f);
   		pos_self=make_float3(0.0f);
   		return 1;
   	}
   	else if(t)
   	{
   		*posAlt=pos+posT/count;
   		*_distTravel=dis;
   		posT=make_float3(0.0f);
   		return 1;
   	}
   	else if(t2)
   	{
   		*posAlt=pos+pos_self/count1;
   		pos_self=make_float3(0.0f);
   		return 1;
   	}
   		return 0;
}

__global__ void	verlet(float4 * pos_vbo, float4 * nor_vbo, float4 * g_pos_in, float4 * g_pos_old_in, float4 * g_pos_out, float4 * g_pos_old_out, int side,

				 float4 *oldPos,
                 uint  *gridParticleIndex,
                 uint  *cellStart,
                 uint  *cellEnd,
                 uint   objs,
				 float4 *face)
{

	/*In you first code sample, since both posData and posOldData are  volatile,  actual memory instructions are generated. One read/write each time you use either A or B.
	 The good point is that other threads will be able to see that modifications earlier, while they are made.
	  The downside is that the execution will be slower, because the caches will be disabled.

	In your second code sample, on the other side, the GPU is authorized to use caches to accelerate its execution,
	until the end of the function, when it's forced to issue a memory write. If both A and B are already cached, only 2 memory writes are issued.
	The downside is that other threads might only be able to see the changed value after the fence.


	SINCE WE DONT NEED IMMEDIATE UPDATE WE USE SECOND ONE TO INCREASE EFFICIENTLY */

    uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if(index >= side*side)return;

	int ix = index % side; 
	int iy = index / side; 

	volatile float4 posData = g_pos_in[index];    // ensure coalesced read
    volatile float4 posOldData = g_pos_old_in[index];

    float3 pos = make_float3(posData.x, posData.y, posData.z);
    float3 pos_old = make_float3(posOldData.x, posOldData.y, posOldData.z);
	float3 vel = (pos - pos_old) / 0.01;

	// used for computation of the normal
	float3 normal = make_float3(0, 0, 0);
	float3 last_diff = make_float3(0, 0, 0);
	float iters = 0.0;
	
	float3 force = make_float3(0.0, -9.81, 0.0);

	float inv_mass =64.0f/(side*side);// params.inverse_mass;
	if(params.hang==1)
	{
		if (index <= (side - 1.0))
			inv_mass = 0.f;
	}
	else if(params.hang==2)
		{
			if (index <= (side/2*side - 1.0) && index>((side/2-1)*side-1))
				inv_mass = 0.f;
		}
	else if(params.hang==3)
	{
		if (index ==(side-1)/2*(side-1)/2)
			inv_mass=0;
	}
	float step = params.step;

	for (int k = 0; k < 12; k++)
	{
		int2 coord = nextNeigh(k);
		int j = coord.x;
		int i = coord.y;

		if (((iy + i) < 0) || ((iy + i) > (side - 1)))
			continue;

		if (((ix + j) < 0) || ((ix + j) > (side - 1)))
			continue;

		int index_neigh = (iy + i) * side + ix + j;

		volatile float4 pos_neighData = g_pos_in[index_neigh];

		float3 pos_neigh = make_float3(pos_neighData.x, pos_neighData.y, pos_neighData.z);
		float3 diff = pos_neigh - pos;
		float3 curr_diff = diff;	// curr diff is the normalized direction of the spring
		curr_diff = normalize(curr_diff);

		if ((iters > 0.0) && (k < 8))
		{
			float an = dot(curr_diff, last_diff);
			if (an > 0.0)
				normal += cross(last_diff, curr_diff);
		}
		last_diff = curr_diff;

		float2 fcoord = make_float2(coord)* step;
		float rest_length = length(fcoord);

		force += (curr_diff * (length(diff) - rest_length)) * params.stiffness - vel * params.globalDamping;
		if (k < 8)
			iters += 1.0;
	}

	normal = normalize(normal / -(iters - 1.0));

	float3 acc = make_float3(0, 0, 0);
	acc = force * inv_mass;

	// verlet
	float3 tmp = pos;
	pos = pos * 2 - pos_old +acc * 0.01 * 0.01 ;
	pos_old = tmp;




	uint count=0;
	float distance=0;
	int3 gridPosT = calcGridPos(pos);
	float3 posAlt=make_float3(0.0f);
	float3 posFin=make_float3(0.0f);
	bool t1=0,t2=0;
	for (int z=-1; z<=1; z++)
      {
         for (int y=-1; y<=1; y++)
          {
             for (int x=-1; x<=1; x++)
              {
                  int3 neighbourPos = gridPosT + make_int3(x, y, z);
                  t1=collideCell(neighbourPos, index, pos, (pos - pos_old) / 0.01,oldPos, cellStart,
                		  cellEnd,side,objs,face,gridParticleIndex,&posAlt,&distance,g_pos_old_in);
				if(t1)
				{

					count++;
					t1=0;
					t2=1;
					posFin+=posAlt;
					posAlt=make_float3(0.0f);
				}



              }
          }
	  }

	if(t2)
	{
		pos=posFin/count;
		posFin=make_float3(0.0f);
	}

	__syncthreads();
	

	pos_vbo[index] = make_float4(pos.x, pos.y, pos.z, posData.w);
	nor_vbo[index] = make_float4(normal.x, normal.y, normal.z, 0.0);

	g_pos_out[index] = make_float4(pos.x, pos.y, pos.z, posData.w);
	g_pos_old_out[index] = make_float4(pos_old.x, pos_old.y, pos_old.z, posOldData.w);
	

}
#endif
