/*
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

// This file contains C wrappers around the some of the CUDA API and the
// kernel functions so that they can be called from "particleSystem.cpp"



#include <cstdlib>
#include <cstdio>
#include <string.h>

#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

#include <helper_cuda.h>
#include <helper_cuda_gl.h>

#include <helper_functions.h>
#include "thrust/device_ptr.h"
#include "thrust/for_each.h"
#include "thrust/iterator/zip_iterator.h"
#include "thrust/sort.h"

#include "particles_kernel_impl.cuh"



float4 *pCudaPos0 = NULL;
float4 *pCudaPos1 = NULL;
float4 *pCudaPosOld0 = NULL; 
float4 *pCudaPosOld1 = NULL;   
float4 *pCudaVel = NULL;

float4 * pPosIn, *pPosOut,*mVel;
float4 * pPosOldIn, *pPosOldOut;
int iters = 0;

extern "C"
{

    void cudaInit(int argc, char **argv)
    {
        int devID;

        // use command-line specified CUDA device, otherwise use device with highest Gflops/s
        devID = findCudaDevice(argc, (const char **)argv);

        if (devID < 0)
        {
            printf("No CUDA Capable devices found, exiting...\n");
            exit(EXIT_SUCCESS);
        }
    
	}

    void cudaGLInit(int argc, char **argv)
    {
        // use command-line specified CUDA device, otherwise use device with highest Gflops/s
        findCudaGLDevice(argc, (const char **)argv);
    }

    void allocateArray(void **devPtr, size_t size)
    {
        checkCudaErrors(cudaMalloc(devPtr, size));
    }

    void freeArray(void *devPtr)
    {
        checkCudaErrors(cudaFree(devPtr));
    }

    void threadSync()
    {
        checkCudaErrors(cudaDeviceSynchronize());
    }

    void copyArrayToDevice(void *device, const void *host, int offset, int size)
    {
        checkCudaErrors(cudaMemcpy((char *) device + offset, host, size, cudaMemcpyHostToDevice));
    }

    void registerGLBufferObject(uint vbo, struct cudaGraphicsResource **cuda_vbo_resource)
    {
        checkCudaErrors(cudaGraphicsGLRegisterBuffer(cuda_vbo_resource, vbo,
                                                     cudaGraphicsMapFlagsNone));
    }

    void unregisterGLBufferObject(struct cudaGraphicsResource *cuda_vbo_resource)
    {
        checkCudaErrors(cudaGraphicsUnregisterResource(cuda_vbo_resource));
    }

    void *mapGLBufferObject(struct cudaGraphicsResource **cuda_vbo_resource)
    {
        void *ptr;
        checkCudaErrors(cudaGraphicsMapResources(1, cuda_vbo_resource, 0));
        size_t num_bytes;
        checkCudaErrors(cudaGraphicsResourceGetMappedPointer((void **)&ptr, &num_bytes,
                                                             *cuda_vbo_resource));
        return ptr;
    }

    void unmapGLBufferObject(struct cudaGraphicsResource *cuda_vbo_resource)
    {
        checkCudaErrors(cudaGraphicsUnmapResources(1, &cuda_vbo_resource, 0));
    }

    void copyArrayFromDevice(void *host, const void *device,
                             struct cudaGraphicsResource **cuda_vbo_resource, int size)
    {
        if (cuda_vbo_resource)
        {
            device = mapGLBufferObject(cuda_vbo_resource);
        }

        checkCudaErrors(cudaMemcpy(host, device, size, cudaMemcpyDeviceToHost));

        if (cuda_vbo_resource)
        {
            unmapGLBufferObject(*cuda_vbo_resource);
        }
    }

    void setParameters(SimParams *hostParams)
    {
        // copy parameters to constant memory
        checkCudaErrors(cudaMemcpyToSymbol(params, hostParams, sizeof(SimParams)));
    }

    //Round a / b to nearest higher integer value
    uint iDivUp(uint a, uint b)
    {
        return (a % b != 0) ? (a / b + 1) : (a / b);
    }

    // compute grid and thread block size for a given number of elements
    void computeGridSize(uint n, uint blockSize, uint &numBlocks, uint &numThreads)
    {
        numThreads = min(blockSize, n);
        numBlocks = iDivUp(n, numThreads);
    }


	void InCuda(const int size)
{  
	const unsigned int num_threads = size / 4;
	const unsigned int mem_size = sizeof(float4) * num_threads;

	// allocate device memory for float4 version
	checkCudaErrors(cudaMalloc((void**) &pCudaPos0, mem_size));	// positions
	checkCudaErrors(cudaMalloc((void**) &pCudaPos1, mem_size));	// positions
	checkCudaErrors(cudaMalloc((void**) &pCudaPosOld0, mem_size));	// old positions
	checkCudaErrors(cudaMalloc((void**) &pCudaPosOld1, mem_size));	// old positions
	iters = 0;
}
//////wrote by Marco Fratarcangeli
	void UploadCuda(float * positions, float * positions_old, const int size)
{
	assert(pCudaPos0 != NULL); 
	assert(pCudaPosOld0 != NULL); 

	const unsigned int num_threads = size;
	//cutilCondition(0 == (num_threads % 4));
	//    checkCudaErrors(0 == (num_threads % 4));
	const unsigned int mem_size = sizeof(float4) * num_threads;
	
	// copy host memory to device
	// NOTE: it is not necessary to copy in each iteration, just swap the buffers -> huge save of computation time

	if ((iters % 2) == 0)   
	{     
		pPosIn = pCudaPos0;			pPosOut = pCudaPos1;
		pPosOldIn = pCudaPosOld0;	pPosOldOut = pCudaPosOld1;
	}
	else
	{
		pPosIn = pCudaPos1;			pPosOut = pCudaPos0;
		pPosOldIn = pCudaPosOld1;	pPosOldOut = pCudaPosOld0; 
	}

	if (iters == 0)
	{
		checkCudaErrors(cudaMemcpy(pPosIn, positions,  mem_size, cudaMemcpyHostToDevice));
		checkCudaErrors(cudaMemcpy(pPosOldIn, positions_old, mem_size, cudaMemcpyHostToDevice));
		getLastCudaError("Cuda memory copy host to device failed.");
	} 

	iters++;
}

	void VerletCuda(float * pos_vbo, float * nor_vbo, const int size, const int & side,
				 float *oldPos,
                 uint  *gridParticleIndex,
                 uint  *cellStart,
                 uint  *cellEnd,
                 uint   numCells,
				 uint   objs,
				 float *face)
{
		uint numParticles = size;

#if USE_TEX
        checkCudaErrors(cudaBindTexture(0, oldPosTex, oldPos, (numParticles+objs)*sizeof(float4)));
        checkCudaErrors(cudaBindTexture(0, cellStartTex, cellStart, numCells*sizeof(uint)));
        checkCudaErrors(cudaBindTexture(0, cellEndTex, cellEnd, numCells*sizeof(uint)));

          checkCudaErrors(cudaBindTexture(0, faceTex, face, objs*3*sizeof(float4)));
#endif
	// setup execution parameters 
	uint numThreads, numBlocks;
	
	computeGridSize(numParticles, 128, numBlocks, numThreads);

	uint smemSize = sizeof(uint)*(12*numThreads);
	// execute the kernel
	verlet<<< numBlocks, numThreads, smemSize>>>((float4 *) pos_vbo,(float4 *) nor_vbo, pPosIn, pPosOldIn, pPosOut, pPosOldOut, side,

											  (float4 *)oldPos,
                                              gridParticleIndex,
                                              cellStart,
                                              cellEnd,
                                              objs,
											  (float4 *)face);

	// stop the CPU until the kernel has been executed
	cudaThreadSynchronize();

	// check if kernel execution generated and error
	getLastCudaError("Cuda kernel execution failed.");
#if USE_TEX
        checkCudaErrors(cudaUnbindTexture(oldPosTex));
        checkCudaErrors(cudaUnbindTexture(cellStartTex));
        checkCudaErrors(cudaUnbindTexture(cellEndTex));
      checkCudaErrors(cudaUnbindTexture(faceTex));
#endif
}
    void calcHash(uint  *gridParticleHash,
                  uint  *gridParticleIndex,
                  int    numParticles,
                  int 	 obj)
    {
        uint numThreads, numBlocks;
       
		computeGridSize(numParticles+obj, 256, numBlocks, numThreads);
	
        // execute the kernel
        calcHashD<<< numBlocks, numThreads >>>(gridParticleHash,
											   gridParticleIndex,
											   pPosIn,
											   numParticles,
						  				 	   obj);

        // check if kernel invocation generated an error
        getLastCudaError("Kernel execution failed");
    }

    void reorderDataAndFindCellStart(uint  *cellStart,
                                     uint  *cellEnd,
                                     float *sortedPos,

                                     uint  *gridParticleHash,
                                     uint  *gridParticleIndex,

                                     uint   numParticles,
                                     uint 	obj,
                                     uint   numCells)
    {
        uint numThreads, numBlocks;
        computeGridSize(numParticles+obj, 256, numBlocks, numThreads);

        // set all cells to empty
        checkCudaErrors(cudaMemset(cellStart, 0xffffffff, numCells*sizeof(uint)));

#if USE_TEX
        checkCudaErrors(cudaBindTexture(0, oldPosTex, pPosIn, (numParticles+obj)*sizeof(float4)));

#endif

        uint smemSize = sizeof(uint)*(numThreads+1);
        reorderDataAndFindCellStartD<<< numBlocks, numThreads, smemSize>>>(
            cellStart,
            cellEnd,
            (float4 *) sortedPos,

            gridParticleHash,
            gridParticleIndex,
            pPosIn,

            numParticles,
            obj);
        getLastCudaError("Kernel execution failed: reorderDataAndFindCellStartD");

#if USE_TEX
        checkCudaErrors(cudaUnbindTexture(oldPosTex));

#endif
    }


    void sortParticles(uint *dGridParticleHash, uint *dGridParticleIndex, uint numParticles)
    {
        thrust::sort_by_key(thrust::device_ptr<uint>(dGridParticleHash),
                            thrust::device_ptr<uint>(dGridParticleHash + numParticles),
                            thrust::device_ptr<uint>(dGridParticleIndex));
    }


}   // extern "C"
