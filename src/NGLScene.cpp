#include <QMouseEvent>
#include <QGuiApplication>

#include "NGLScene.h"
#include <ngl/NGLInit.h>


const static int THREAD = 0;
const static int num_thrd =8;
const static int  GRID_SIZE  =     64;		// Hash grid resolution
const static int NUM_PARTICLES =  128*128;  // cloth particles structure (32*32),(64*64),(128*128)
const static int bbx=0;        //// using (1) simple bounding sphere or (0) the most efficient one
const static float INCREMENT=0.01;
//----------------------------------------------------------------------------------------------------------------------
/// @brief the increment for the wheel zoom
//----------------------------------------------------------------------------------------------------------------------
const static float ZOOM=0.1;


NGLScene::NGLScene(QWindow *_parent) : OpenGLWindow(_parent),m_rotate(false),  m_spinXFace(0),
    m_spinYFace(0)
{
  // re-size the widget to that of the parent (in this case the GLFrame passed in on construction)
  setTitle("GPGPU Cloth");
}


NGLScene::~NGLScene()
{
  delete renderer;
  delete m_light;
  delete[] midBound;
  delete[] tri;
  ngl::NGLInit *Init = ngl::NGLInit::instance();
  std::cout<<"Shutting down NGL, removing VAO's and Shaders\n";
  Init->NGLQuit();
}

void NGLScene::resizeEvent(QResizeEvent *_event )
{
  if(isExposed())
  {
  int w=_event->size().width();
  int h=_event->size().height();
  // set the viewport for openGL
  glViewport(0,0,w,h);
  m_cam->setShape(45,(float)w/h,0.05,350);

  renderLater();
  }
}


void NGLScene::initialize()
{


  ngl::NGLInit::instance();
  glClearColor(0.25, 0.25, 0.25, 1.0);	   // Grey Background
  // enable depth testing for drawing
  glEnable(GL_DEPTH_TEST);
  // enable multisampling for smoother drawing
  glEnable(GL_MULTISAMPLE);
  // as re-size is not explicitly called we need to do this.
  glViewport(0,0,width(),height());

  ngl::Vec3 from(0,40,-40);
  ngl::Vec3 to(0,10,0);
  ngl::Vec3 up(0,1,0);
  // now load to our new camera
  m_cam= new ngl::Camera(from,to,up);
  // set the shape using FOV 45 Aspect Ratio based on Width and Height
  // The final two are near and far clipping planes of 0.5 and 10
  m_cam->setShape(45,(float)720.0/576.0,0.05,350);


  ngl::ShaderLib *shader=ngl::ShaderLib::instance();

  shader->createShaderProgram("CLR");
  // now we are going to create empty shaders for Frag and Vert
  shader->attachShader("CLRVertex",ngl::VERTEX);
  shader->attachShader("CLRFragment",ngl::FRAGMENT);
  // attach the source
  shader->loadShaderSource("CLRVertex","shaders/colourvs.glsl");
  shader->loadShaderSource("CLRFragment","shaders/colourfs.glsl");
  // compile the shaders
  shader->compileShader("CLRVertex");
  shader->compileShader("CLRFragment");
  // add them to the program
  shader->attachShaderToProgram("CLR","CLRVertex");
  shader->attachShaderToProgram("CLR","CLRFragment");
  // now bind the shader attributes for most NGL primitives we use the following
  // layout attribute 0 is the vertex data (x,y,z)
  shader->bindAttribute("CLR",0,"inVert");
  // attribute 1 is the UV data u,v (if present)
  // attribute 2 are the normals x,y,z

  shader->linkProgramObject("CLR");


  shader->createShaderProgram("Fabric");
  // now we are going to create empty shaders for Frag and Vert
  shader->attachShader("FabricVertex",ngl::VERTEX);
  shader->attachShader("FabricFragment",ngl::FRAGMENT);
  // attach the source
  shader->loadShaderSource("FabricVertex","shaders/PhongVertex.glsl");
  shader->loadShaderSource("FabricFragment","shaders/PhongFragment.glsl");
  // compile the shaders
  shader->compileShader("FabricVertex");
  shader->compileShader("FabricFragment");
  // add them to the program
  shader->attachShaderToProgram("Fabric","FabricVertex");
  shader->attachShaderToProgram("Fabric","FabricFragment");
  // now bind the shader attributes for most NGL primitives we use the following
  // layout attribute 0 is the vertex data (x,y,z)
  shader->bindAttribute("Fabric",0,"inVert");
  // attribute 1 is the UV data u,v (if present)
  // attribute 2 are the normals x,y,z

  shader->bindAttribute("Fabric",2,"inUV");

  shader->bindAttribute("Fabric",1,"inNormal");
  shader->linkProgramObject("Fabric");


//  ngl::ShaderLib *shader=ngl::ShaderLib::instance();
  // we are creating a shader called Phong
  shader->createShaderProgram("Phong");
  // now we are going to create empty shaders for Frag and Vert
  shader->attachShader("PhongVertex",ngl::VERTEX);
  shader->attachShader("PhongFragment",ngl::FRAGMENT);
  // attach the source
  shader->loadShaderSource("PhongVertex","shaders/PhongVertex.glsl");
  shader->loadShaderSource("PhongFragment","shaders/PhongFragment.glsl");
  // compile the shaders
  shader->compileShader("PhongVertex");
  shader->compileShader("PhongFragment");
  // add them to the program
  shader->attachShaderToProgram("Phong","PhongVertex");
  shader->attachShaderToProgram("Phong","PhongFragment");
  // now bind the shader attributes for most NGL primitives we use the following
  // layout attribute 0 is the vertex data (x,y,z)
  shader->bindAttribute("Phong",0,"inVert");
  // attribute 1 is the UV data u,v (if present)
  // attribute 2 are the normals x,y,z
  shader->bindAttribute("Phong",1,"inNormal");
  shader->linkProgramObject("Phong");
  // and make it active ready to load values
  (*shader)["Phong"]->use();
  // the shader will use the currently active material and light0 so set them
  ngl::Material m(ngl::GOLD);
  // load our material values to the shader into the structure material (see Vertex shader)
  m.loadToShader("material");
  shader->setShaderParam3f("viewerPos",m_cam->getEye().m_x,m_cam->getEye().m_y,m_cam->getEye().m_z);

  ngl::Mat4 iv=m_cam->getViewMatrix();
  iv.transpose();
  m_light = new ngl::Light(ngl::Vec3(-1,35,-45),ngl::Colour(1,1,1,1),ngl::Colour(1,1,1,1),ngl::POINTLIGHT );
  m_light->setTransform(iv);
  // load these values to the shader as well
  m_light->loadToShader("light");

  initParticleSystem();

}



void NGLScene::render()
{
  // clear the screen and depth buffer
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // update the simulation
    if (!m_bPause)
      {
    psystem->update();
          if (renderer)
          {
           renderer->setVertexBuffer(psystem->getCurrentReadBuffer(),psystem->getCurrentNormBuffer(),
           psystem->getCurrentcoordBuffer(),psystem->getCurrentFaceBuffer(), psystem->getNumParticles(),m_bWireframe);
          }
      }

      ngl::Mat4 rotX;
      ngl::Mat4 rotY;
      // create the rotation matrices
      rotX.rotateX(m_spinXFace);
      rotY.rotateY(m_spinYFace);
      // multiply the rotations
      m_mouseGlobalTX=rotY*rotX;
      // add the translations
      m_mouseGlobalTX.m_m[3][0] = m_modelPos.m_x;
      m_mouseGlobalTX.m_m[3][1] = m_modelPos.m_y;
      m_mouseGlobalTX.m_m[3][2] = m_modelPos.m_z;

      loadMatricesToShader("Phong");
      obj.Draw();

      loadMatricesToShader("Phong");

      renderer->DrawParticleSystem();
}

void NGLScene::loadMatricesToShader(const std::string & _shader)
{
  ngl::ShaderLib *shader=ngl::ShaderLib::instance();
  shader->use(_shader);
  ngl::Mat4 MV;
  ngl::Mat4 MVP;
  ngl::Mat3 normalMatrix;
  ngl::Mat4 M;
  M=m_transformStack.getCurrentTransform().getMatrix()*m_mouseGlobalTX;
  MV=  M*m_cam->getViewMatrix();
  MVP= M*m_cam->getVPMatrix();
  normalMatrix=MV;
  normalMatrix.inverse();
  shader->setShaderParamFromMat4("MV",MV);
  shader->setShaderParamFromMat4("MVP",MVP);
  shader->setShaderParamFromMat3("normalMatrix",normalMatrix);
  shader->setShaderParamFromMat4("M",M);
}



//----------------------------------------------------------------------------------------------------------------------
void NGLScene::mouseMoveEvent (QMouseEvent * _event)
{
    // note the method buttons() is the button state when event was called
    // this is different from button() which is used to check which button was
    // pressed when the mousePress/Release event is generated
    if(m_rotate && _event->buttons() == Qt::LeftButton)
    {
      int diffx=_event->x()-m_origX;
      int diffy=_event->y()-m_origY;
      m_spinXFace -= (float) 0.5f * diffy;
      m_spinYFace += (float) 0.5f * diffx;
      m_origX = _event->x();
      m_origY = _event->y();
      renderLater();

    }
          // right mouse translate code
    else if(m_translate && _event->buttons() == Qt::RightButton)
    {
      int diffX = (int)(_event->x() - m_origXPos);
      int diffY = (int)(_event->y() - m_origYPos);
      m_origXPos=_event->x();
      m_origYPos=_event->y();
      m_modelPos.m_x -= INCREMENT * diffX;
      m_modelPos.m_y -= INCREMENT * diffY;
      renderLater();

     }
}


//----------------------------------------------------------------------------------------------------------------------
void NGLScene::mousePressEvent ( QMouseEvent * _event)
{
    if(_event->button() == Qt::LeftButton)
    {
      m_origX = _event->x();
      m_origY = _event->y();
      m_rotate =true;
    }
    // right mouse translate mode
    else if(_event->button() == Qt::RightButton)
    {
      m_origXPos = _event->x();
      m_origYPos = _event->y();
      m_translate=true;
    }
}

//----------------------------------------------------------------------------------------------------------------------
void NGLScene::mouseReleaseEvent ( QMouseEvent * _event )
{
    if (_event->button() == Qt::LeftButton)
    {
      m_rotate=false;
    }
          // right mouse translate mode
    if (_event->button() == Qt::RightButton)
    {
      m_translate=false;
    }
}

//----------------------------------------------------------------------------------------------------------------------
void NGLScene::wheelEvent(QWheelEvent *_event)
{
    if(_event->delta() > 0)
    {
        m_modelPos.m_z-=ZOOM;
    }
    else if(_event->delta() <0 )
    {
        m_modelPos.m_z+=ZOOM;
    }
    renderLater();
}
//----------------------------------------------------------------------------------------------------------------------

void NGLScene::keyPressEvent(QKeyEvent *_event)
{
  // this method is called every time the main window recives a key event.
  // we then switch on the key value and set the camera in the GLWindow
  switch (_event->key())
  {
  // escape key to quite
  case Qt::Key_Escape : QGuiApplication::exit(EXIT_SUCCESS); break;
 // case Qt::Key_W : m_wirefram==0 ? 1|0; break;

  default : break;
  }
  // finally update the GLWindow and re-draw
  if (isExposed())
    renderLater();
}


void NGLScene::initParticleSystem()
{
   numParticles = NUM_PARTICLES;
   uint gridDim = GRID_SIZE;
   double** ap = new double*[3];
   typedef double* const* PointIterator;
   typedef const double* CoordIterator;
   ///you ccan choose one of this objects
   ///rubberduck5                           <<number of vertexs>> 1500
   ///Teapot                                <<number of vertexs>> 430
   ///CubeBig                               <<number of vertexs>> 1000

   obj.Load("data/Teapot.obj");
   obj.initBuf();

   int totla_triangle=obj.getTotalTriangle();
   int m_size=totla_triangle/9*4;
   midBound=new float[m_size];
   memset(midBound,0,m_size*sizeof(float));
   tri=new float[totla_triangle/3*4];

    #if(!THREAD)

            float cellSize=0;
            int j=0,k=0,c=0,cc=0;
            if(!bbx)
            {
                for(int i=0;i<totla_triangle/3;i++)
                {
                    double *p= new double[3];
                    tri[cc]  =p[0] =(obj.getFacesArr(j));
                    tri[cc+1]=p[1] =(obj.getFacesArr(j+1));
                    tri[cc+2]=p[2] =(obj.getFacesArr(j+2));
                    tri[cc+3]=1;
                    j+=3;
                    cc+=4;
                    ap[c]=p;
                    if(c==2)
                    {
                        typedef Miniball::Miniball <Miniball::CoordAccessor<PointIterator, CoordIterator> > MB;
                        MB mb (3, ap, ap+3);
                        double rad=mb.squared_radius();
                          if(rad> cellSize)
                            cellSize=rad;

                        const double* center = mb.center();
                        midBound[k]=center[0];
                        midBound[k+1]=center[1];
                        midBound[k+2]=center[2];
                        midBound[k+3]=rad;
                        k+=4;

                        for(int i=0; i <= c; i++)
                        {
                          delete[] ap[i];
                        }

                       c=0;

                    }
                    else
                        c++;
                }
                delete[] ap;

            }
            else
            {
                int jj=0;
                float p[9];
                for(int i=0;i<totla_triangle/3;i++)
                        {

                            p[jj]   =(obj.getFacesArr(j));
                            p[jj+1] =(obj.getFacesArr(j+1));
                            p[jj+2] =(obj.getFacesArr(j+2));
                            j+=3;
                            jj+=3;
                            if(c==2)
                            {
                                bounding b_sphere;
                                float4 tem= b_sphere.set_bb(p);
                                if(tem.w> cellSize)
                                        cellSize=tem.w;
                                midBound[k]=tem.x;
                                midBound[k+1]=tem.y;
                                midBound[k+2]=tem.z;
                                midBound[k+3]=tem.w;
                                k+=4;
                                c=0;
                                jj=0;
                            }
                            else
                                c++;
                    }

            }
    #else
        radAr=new float[num_thrd];
        memset(radAr,0,num_thrd*sizeof(float));
        thread = (pthread_t*) malloc(num_thrd*sizeof(pthread_t));
        int i;
        for (i = 0; i < num_thrd; i++)
            {
            // creates each thread working on its own slice of i
                if (pthread_create (&thread[i], NULL, bound, (void*)i) != 0 )
                {
                    perror("Can't create thread");
                    free(thread);
                    exit(-1);
                }
            }

        for (i = 0; i < num_thrd; i++)
            pthread_join (thread[i], NULL);
        free(thread);

        for (i = 0; i < num_thrd; i++)
            if(cellSize<radAr[i])
                cellSize=radAr[i];
    #endif
    ////////////////////////////////////////////////


    gridSize.x = gridSize.y = gridSize.z = gridDim;
    std::cout<<"grid: "<<gridSize.x<<" x "<< gridSize.y<<" x "<<  gridSize.z<<" = "<< gridSize.x*gridSize.y*gridSize.z<<" cells"<<std::endl;
    std::cout<<"particles: "<< numParticles<<std::endl;
    psystem = new ParticleSystem(numParticles, gridSize, midBound,m_size,cellSize,tri,THREAD);
    psystem->reset();
    renderer = new ParticleRenderer;
    renderer->initializeGLFunctions();
}
