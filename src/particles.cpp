///*
// * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
// *
// * Please refer to the NVIDIA end user license agreement (EULA) associated
// * with this source code for terms and conditions that govern your use of
// * this software. Any use, reproduction, disclosure, or distribution of
// * this software and related documentation outside the terms of the EULA
// * is strictly prohibited.
// *
// */

//// OpenGL Graphics includes
//#include <GL/glew.h>
//#include "Obj.h"
//#include "Miniball.hpp"
//// CUDA utilities and system includes
//#include <helper_functions.h>
//#include <helper_cuda.h>    // includes cuda.h and cuda_runtime_api.h
//#include <helper_cuda_gl.h> // includes cuda_gl_interop.h// includes cuda_gl_interop.h
 
//#include "particleSystem.h"
//#include "render_particles.h"
//#include "paramgl.h"

//#include <pthread.h>
//#include "bounding.h"


//////////////////			Pthread

//const int THREAD = 0;
//const int num_thrd =8;    ///Note just designed to work with 2 or 4 or 8 threads
//																				/*If by changing object program becomes slow just try to change num_thrd
//																				 * Sometimes for different object it needs to change */
//pthread_t* thread;
//float* radAr;
//int from=0,to=0,temp=0;
/////////////////
//#define bbx 0		//// using (1) simple bounding sphere or (0) the most efficient one

/////////////////////////////////////////////////////////////////////////////
//const int  GRID_SIZE  =     64;		// Hash grid resolution
//const int NUM_PARTICLES =  128*128;  // cloth particles structure (32*32),(64*64),(128*128)

//const uint width = 640, height = 480; ///Window size

////obj
//Model_OBJ obj;
//float *midBound;
//float* tri;		/* triangle array by default for each vertex has 3 parameter(x,y,z)
//				*and since we want to define it as Texture array 1 to accelerate the
//				*and process we need to redefine it with one extra parameter like this(x,y,z,w)
//				*we can only define texture float,texture float2 and texture float4*/
//// view params
//int ox, oy;
//int buttonState = 0;
//float camera_trans[] = {0, -5, -30};
//float camera_rot[]   = {0, 0, 0};
//float camera_trans_lag[] = {0, 0, -5};
//float camera_rot_lag[] = {0, 0, 0};
//const float inertia = 0.1f;

////ParticleRenderer::DisplayMode displayMode = ParticleRenderer::CLOTH;

//int mode = 0;
//bool displayEnabled = true;
//bool bPause = false;
//bool displaySliders = false;
//bool wireframe = false;
//bool demoMode = false;
//int idleCounter = 0;
//int demoCounter = 0;
//const int idleDelay = 2000;

//enum { M_VIEW = 0, M_MOVE };

//uint numParticles = 0;
//uint3 gridSize;



//// simulation parameters
//float timestep = 0.5f;
//float damping = 1.0f;
//float gravity = 0.98f;
//int iterations =4;
//int ballr = 10;

//float collideSpring = 0.5f;
//float collideDamping = 0.02f;
//float collideShear = 0.1f;
//float collideAttraction = 0.0f;

//ParticleSystem *psystem = 0;

//// fps
//static int fpsCount = 0;
//static int fpsLimit = 1;
//StopWatchInterface *timer = NULL;

//ParticleRenderer *renderer = 0;

//float modelView[16];

//ParamListGL *params;

//// Auto-Verification Code
//unsigned int frameCount = 0;
//unsigned int g_TotalErrors = 0;
//char        *g_refFile = NULL;

//const char *sSDKsample = "CUDA CLoth Simulation";

//extern "C" void cudaGLInit(int argc, char **argv);

//// initialize particle system

//void initParticleSystem(int numParticles, uint3 gridSize, float *b_mid,int size,int cellsize,float *facelist)
//{
//    psystem = new ParticleSystem(numParticles, gridSize, b_mid,size,cellsize,facelist,THREAD);
//    psystem->reset();
//    renderer = new ParticleRenderer;

//    sdkCreateTimer(&timer);
//}



////void computeFPS()
////{
////    frameCount++;
////    fpsCount++;

////    if (fpsCount == fpsLimit)
////    {
////        char fps[256];
////        float ifps = 1.f / (sdkGetAverageTimerValue(&timer) / 1000.f);
////        sprintf(fps, "CCD Cloth (%d particles): %3.1f fps", numParticles, ifps);

////        glutSetWindowTitle(fps);
////        fpsCount = 0;

////        fpsLimit = (int)MAX(ifps, 1.f);
////        sdkResetTimer(&timer);
////    }
////}

//void display()
//{
//    sdkStartTimer(&timer);

//    // update the simulation
//    if (!bPause)
//    {
//        psystem->update();
//        if (renderer)
//        {
//           renderer->setVertexBuffer(psystem->getCurrentReadBuffer(),psystem->getCurrentNormBuffer(),
//		   psystem->getCurrentcoordBuffer(),psystem->getCurrentFaceBuffer(), psystem->getNumParticles(),wireframe);
//        }
//    }

//    // render
//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

//    // view transform
//    glMatrixMode(GL_MODELVIEW);
//    glLoadIdentity();

//    for (int c = 0; c < 3; ++c)
//    {
//        camera_trans_lag[c] += (camera_trans[c] - camera_trans_lag[c]) * inertia;
//        camera_rot_lag[c] += (camera_rot[c] - camera_rot_lag[c]) * inertia;
//    }

//    glTranslatef(camera_trans_lag[0], camera_trans_lag[1], camera_trans_lag[2]);
//    glRotatef(camera_rot_lag[0], 1.0, 0.0, 0.0);
//    glRotatef(camera_rot_lag[1], 0.0, 1.0, 0.0);

//    glGetFloatv(GL_MODELVIEW_MATRIX, modelView);



//    // Collide
//    glPushMatrix();
//    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//    float3 p = psystem->getColliderPos();
//	glScalef(0.95f,0.95f,0.95f);
//	glTranslatef(p.x, p.y, p.z);
//   // glColor3f(0.3, 0.4, 0.9);
//    obj.Draw();
//	glPopMatrix();

//    if (renderer && displayEnabled)
//    {
//        renderer->display();
//    }
	
//    sdkStopTimer(&timer);

//    glutSwapBuffers();
//    glutReportErrors();

//    computeFPS();
//}




/////thread

//void* bound(void* slice)
//{
//  int add;
//  int total_triangle=obj.getTotalTriangle();
//  intptr_t s = (long)slice;   // retrive the slice info
//  if(s)
//  {
//	  from=to;
//  	  add=temp;
//  }
//  else
//  {
//	from = (s * total_triangle/3)/num_thrd; // note that this 'slicing' works fine
//	add=0;
//  }
//  to = ((s+1) * total_triangle/3)/num_thrd; // even if SIZE is not divisible by num_thrd
//  if(to%3)
//  {
//  	  temp=3-(to%3);
//  	  to+=temp;
//  }
//  else
//	  temp=0;
//  if(to>total_triangle/3)
//	  	  to=total_triangle/3;
//#if(!bbx)

//  double** ap = new double*[3];
//  typedef double* const* PointIterator;
//  typedef const double*  CoordIterator;
//  int j=0,k=0,c=0,cc=0;

//  for (int i = from; i < to; i++)
//  {
//	  	  	  	double p[3];

//                        tri[4*(total_triangle*s+add)/3/num_thrd+cc]  =p[0] =(obj.getFacesArr((total_triangle*s+add)/num_thrd+j));
//	  	  	  			tri[4*(total_triangle*s+add)/3/num_thrd+cc+1]=p[1] =(obj.getFacesArr((total_triangle*s+add)/num_thrd+j+1));
//	  	  	  			tri[4*(total_triangle*s+add)/3/num_thrd+cc+2]=p[2] =(obj.getFacesArr((total_triangle*s+add)/num_thrd+j+2));
//	  	  				tri[4*(total_triangle*s+add)/3/num_thrd+cc+3]=1;
//	  			j+=3;
//	  			ap[c]=p;
//	  			cc+=4;
//	  			if(c==2)
//	  				{
//	  				    typedef Miniball::Miniball <Miniball::CoordAccessor<PointIterator, CoordIterator> > MB;
//	  					MB mb (3, ap, ap+3);
//	  					double rad=mb.squared_radius();
//	  					if(rad> radAr[s])
//	  							radAr[s]=rad;
//	  					const double* center = mb.center();

//	  					midBound[4*(total_triangle*s+add)/9/num_thrd+k  ]=center[0];
//	  					midBound[4*(total_triangle*s+add)/9/num_thrd+k+1]=center[1];
//	  					midBound[4*(total_triangle*s+add)/9/num_thrd+k+2]=center[2];
//	  					midBound[4*(total_triangle*s+add)/9/num_thrd+k+3]=rad;
//	  					k+=4;
//	  					c=0;
//	  				}
//	  			else
//	  					c++;
//  }
//#else
//  int j=0,k=0,t=0,c=0,jj=0,cc=0;
//   float p[9];
//   for (int i = from; i < to; i++)
//   {
//                    tri[cc]  =p[jj]   =(obj.getFacesArr((total_triangle*s+add)/num_thrd+j));
//                    tri[cc+1]=p[jj+1] =(obj.getFacesArr((total_triangle*s+add)/num_thrd+j+1));
//                    tri[cc+2]=p[jj+2] =(obj.getFacesArr((total_triangle*s+add)/num_thrd+j+2));
// 	  	  	  		tri[cc+3]=1;
// 	  	  	  	jj+=3;
// 	  			j+=3;
// 	  			cc+=4;
// 	  			if(c==2)
// 	  				{
// 	  					bounding b_sphere;
// 	  					float4 tem= b_sphere.set_bb(p);
// 	  					if(tem.w> radAr[s])
// 	  							radAr[s]=tem.w;
// 	  					//memset(p,0,9*sizeof(float));
//                        midBound[4*(total_triangle*s+add)/9/num_thrd+k  ]=tem.x;
//                        midBound[4*(total_triangle*s+add)/9/num_thrd+k+1]=tem.y;
//                        midBound[4*(total_triangle*s+add)/9/num_thrd+k+2]=tem.z;
//                        midBound[4*(total_triangle*s+add)/9/num_thrd+k+3]=tem.w;
// 	  					k+=4;
// 	  					c=0;
// 	  					jj=0;
// 	  				}
// 	  			else
// 	  					c++;
//   }

//#endif
//  return 0;
//}

//////////////////////////////////////////////////////////////////////////////////
//// Program main
//////////////////////////////////////////////////////////////////////////////////
////int main(int argc, char **argv)
////{

////    std::cout<<"%s Starting...\n\n"<< sSDKsample<<std::endl;
////    numParticles = NUM_PARTICLES;
////    uint gridDim = GRID_SIZE;

////		double** ap = new double*[3];
////		typedef double* const* PointIterator;
////		typedef const double* CoordIterator;
////		///most minimal sphere
////		///rubberduck5 1500
////		///Teapot 430
////		///CubeBig 1000
////		//bunny01
////        obj.Load("data/rubberduck5.obj");
////		int totla_triangle=obj.getTotalTriangle();
////        int m_size=totla_triangle/9*4;
////		midBound=new float[m_size];
////		memset(midBound,0,m_size*sizeof(float));
////		tri=new float[totla_triangle/3*4];

////#if(!THREAD)

////        int j=0,k=0,c=0,cc=0;
////         float cellSize=0;
////		if(!bbx)
////		{
////			for(int i=0;i<totla_triangle/3;i++)
////			{
////				double *p= new double[3];
////				tri[cc]  =p[0] =(obj.getFacesArr(j));
////				tri[cc+1]=p[1] =(obj.getFacesArr(j+1));
////				tri[cc+2]=p[2] =(obj.getFacesArr(j+2));
////				tri[cc+3]=1;
////				j+=3;
////				cc+=4;
////				ap[c]=p;

////				if(c==2)
////				{
////					typedef Miniball::Miniball <Miniball::CoordAccessor<PointIterator, CoordIterator> > MB;
////					MB mb (3, ap, ap+3);
////					double rad=mb.squared_radius();
////					  if(rad> cellSize)
////						cellSize=rad;

////					const double* center = mb.center();
////					midBound[k]=center[0];
////					midBound[k+1]=center[1];
////					midBound[k+2]=center[2];
////					midBound[k+3]=rad;
////					k+=4;

////                    for(int i=0; i <= c; i++)
////                    {
////                      delete[] ap[i];
////                    }

////                   c=0;

////				}
////				else
////					c++;
////			}
////            delete[] ap;

////        }
////		else
////		{
////			int jj=0;
////			float p[9];
////			for(int i=0;i<totla_triangle/3;i++)
////					{

////						p[jj]   =(obj.getFacesArr(j));
////						p[jj+1] =(obj.getFacesArr(j+1));
////						p[jj+2] =(obj.getFacesArr(j+2));
////						j+=3;
////						jj+=3;
////						if(c==2)
////						{
////							bounding b_sphere;
////							float4 tem= b_sphere.set_bb(p);
////							if(tem.w> cellSize)
////									cellSize=tem.w;
////							midBound[k]=tem.x;
////							midBound[k+1]=tem.y;
////							midBound[k+2]=tem.z;
////							midBound[k+3]=tem.w;
////							k+=4;
////							c=0;
////							jj=0;
////						}
////						else
////							c++;
////				}

////		}
////#else
////    radAr=new float[num_thrd];
////	memset(radAr,0,num_thrd*sizeof(float));
////	thread = (pthread_t*) malloc(num_thrd*sizeof(pthread_t));
////	int i;
////	for (i = 0; i < num_thrd; i++)
////		{
////		// creates each thread working on its own slice of i
////			if (pthread_create (&thread[i], NULL, bound, (void*)i) != 0 )
////			{
////				perror("Can't create thread");
////				free(thread);
////				exit(-1);
////			}
////		}

////	for (i = 0; i < num_thrd; i++)
////		pthread_join (thread[i], NULL);
////	free(thread);

////	for (i = 0; i < num_thrd; i++)
////		if(cellSize<radAr[i])
////			cellSize=radAr[i];
////#endif
////////////////////////////////////////////////////


////    gridSize.x = gridSize.y = gridSize.z = gridDim;
////    printf("grid: %d x %d x %d = %d cells\n", gridSize.x, gridSize.y, gridSize.z, gridSize.x*gridSize.y*gridSize.z);
////    std::cout<<"particles: "<< numParticles<<std::endl;
////    initGL(&argc, argv);
////    cudaGLInit(argc, argv);
////	initParticleSystem(numParticles, gridSize,midBound,m_size,cellSize,tri);
////    glutDisplayFunc(display);
////    glutReshapeFunc(reshape);
////    glutMouseFunc(mouse);
////    glutMotionFunc(motion);
////    glutKeyboardFunc(key);
////    glutSpecialFunc(special);
////    glutIdleFunc(idle);

////    atexit(cleanup);

////    glutMainLoop();


////    if (psystem)
////    {
////        delete psystem;
////    }

////    cudaDeviceReset();
////    exit(g_TotalErrors > 0 ? EXIT_FAILURE : EXIT_SUCCESS);
////}

































