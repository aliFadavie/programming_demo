#include "Obj.h"


Model_OBJ::Model_OBJ():TotalConnectedTriangles(0),TotalConnectedPoints(0),vertexBuf(0),normalBuf(0),indBuf(0),m_vao(0){}


Model_OBJ::~Model_OBJ(){


}


int Model_OBJ::Load(char* filename)
{
    initializeGLFunctions();

    std::string line;
	std::ifstream objFile (filename);
	if (objFile.is_open())													// If obj file is open, continue
	{
		objFile.seekg (0, std::ios::end);									// Go to end of the file,
		long fileSize = objFile.tellg();									// get file size
		objFile.seekg (0, std::ios::beg);									// we'll use this to register memory for our 3d model

		Faces_Triangles.resize(fileSize,0);
        Faces_normals.resize(fileSize,0);

        int triangle_index = 0;												// Set triangle index to zero
        unsigned short count=0;
		while (! objFile.eof() )											// Start reading file data
		{
			getline (objFile,line);											// Get line from file

            if (line.c_str()[0] == 'v' && line.c_str()[1]==' ')										// The first character is a v: on this line is a vertex stored.
			{
				line[0] = ' ';												// Set first character to 0. This will allow us to use sscanf
                float x=0,y=0,z=0;
                sscanf(line.c_str(),"%f %f %f ",&x,&y,&z);							// Read floats from the line: v X Y Z

            vertexBuffer.push_back(x);
            vertexBuffer.push_back(y);
            vertexBuffer.push_back(z);
                                    // Add 3 to the total connected points
			}
            TotalConnectedPoints =vertexBuffer.size();
            if (line.c_str()[0] == 'v' && line.c_str()[1]=='n')										// The first character is a v: on this line is a vertex stored.
            {
                line[0] = ' ';
                line[1] = ' ';
                float x=0,y=0,z=0;
                sscanf(line.c_str(),"%f %f %f ",&x,&y,&z);
                normals.push_back(x);
                normals.push_back(y);
                normals.push_back(z);
            }

            if (line.c_str()[0] == 'f')										// The first character is an 'f': on this line is a point stored
			{
		    	line[0] = ' ';												// Set first character to 0. This will allow us to use sscanf

                int vertexNumber[3],x,normalNum[3];
                sscanf(line.c_str(),"%i/%d/%i %i/%d/%i %i/%d/%i",&vertexNumber[0],&x,&normalNum[0], &vertexNumber[1],
                &x,&normalNum[1],&vertexNumber[2],&x,&normalNum[2]);


				//sscanf(line.c_str(),"%i%i%i",								// Read integers from the line:  f 1 2 3
				//	&vertexNumber[0],										// First point of our triangle. This is an
				//	&vertexNumber[1],										// pointer to our vertexBuffer list
				//	&vertexNumber[2] );										// each point represents an X,Y,Z.

                normalNum[0]--;
                normalNum[1]--;
                normalNum[2]--;
                vertexNumber[0] -= 1;										// OBJ file starts counting from 1
				vertexNumber[1] -= 1;										// OBJ file starts counting from 1
				vertexNumber[2] -= 1;										// OBJ file starts counting from 1


				/********************************************************************
				 * Create triangles (f 1 2 3) from points: (v X Y Z) (v X Y Z) (v X Y Z).
				 * The vertexBuffer contains all verteces
				 * The triangles will be created using the verteces we read previously
				 */
                indexBuf.push_back(count);
                count++;
                int tCounter = 0;
				for (int i = 0; i < 3; i++)
				{
                    Faces_Triangles[triangle_index + tCounter    ] = vertexBuffer[3*vertexNumber[i] ];
					Faces_Triangles[triangle_index + tCounter +1 ] = vertexBuffer[3*vertexNumber[i]+1 ];
					Faces_Triangles[triangle_index + tCounter +2 ] = vertexBuffer[3*vertexNumber[i]+2 ];

                    Faces_normals[triangle_index + tCounter    ] = normals[3*normalNum[i] ];
                    Faces_normals[triangle_index + tCounter +1 ] = normals[3*normalNum[i]+1 ];
                    Faces_normals[triangle_index + tCounter +2 ] = normals[3*normalNum[i]+2 ];
                    tCounter += 3;

				}

				/*********************************************************************
				 * Calculate all normals, used for lighting
				 */
//                float coord1[3] = {Faces_Triangles[triangle_index], Faces_Triangles[triangle_index+1],Faces_Triangles[triangle_index+2]};
//				float coord2[3] = {Faces_Triangles[triangle_index+3],Faces_Triangles[triangle_index+4],Faces_Triangles[triangle_index+5]};
//				float coord3[3] = {Faces_Triangles[triangle_index+6],Faces_Triangles[triangle_index+7],Faces_Triangles[triangle_index+8]};
//                //float *norm = calculateNormal( coord1, coord2, coord3 );



//                float va[3], vb[3], vr[3], val;
//                va[0] = coord1[0] - coord2[0];
//                va[1] = coord1[1] - coord2[1];
//                va[2] = coord1[2] - coord2[2];

//                vb[0] = coord1[0] - coord3[0];
//                vb[1] = coord1[1] - coord3[1];
//                vb[2] = coord1[2] - coord3[2];

//                /* cross product */
//                vr[0] = va[1] * vb[2] - vb[1] * va[2];
//                vr[1] = vb[0] * va[2] - va[0] * vb[2];
//                vr[2] = va[0] * vb[1] - vb[0] * va[1];

//                /* normalization factor */
//                val = sqrt( vr[0]*vr[0] + vr[1]*vr[1] + vr[2]*vr[2] );
//                vr[0]/=val;
//                vr[1]/=val;
//                vr[2]/=val;

//				tCounter = 0;
//                //for (int i = 0; i < 3; i++)
//                //{
//                    normals.push_back(vr[0]);
//                    normals.push_back(vr[1]);
//                    normals.push_back(vr[2]);
//					tCounter += 3;
//                //}
				triangle_index += 9;
    //			normal_index += 9;
				TotalConnectedTriangles += 9;
			}

		}
        Faces_Triangles.resize(indexBuf.size()*9);
        Faces_normals.resize(indexBuf.size()*9);

        objFile.close();														// Close OBJ file
	}
	else
	{
		std::cout << "Unable to open file";
	}
	return 0;
}



void Model_OBJ::initBuf()
{
//      ngl::ShaderLib *shader=ngl::ShaderLib::instance();
//      // we are creating a shader called Phong
//      shader->createShaderProgram("Clr");
//      // now we are going to create empty shaders for Frag and Vert
//      shader->attachShader("ClrVertex",ngl::VERTEX);
//      shader->attachShader("ClrFragment",ngl::FRAGMENT);
//      // attach the source
//      shader->loadShaderSource("ClrVertex","shaders/colourvs.glsl");
//      shader->loadShaderSource("ClrFragment","shaders/colourfs.glsl");
//      // compile the shaders
//      shader->compileShader("ClrVertex");
//      shader->compileShader("ClrFragment");
//      // add them to the program
//      shader->attachShaderToProgram("Clr","ClrVertex");
//      shader->attachShaderToProgram("Clr","ClrFragment");
//      shader->bindAttribute("Clr",0,"inVert");
//      // attribute 1 are the normals x,y,z
//    //  shader->bindAttribute("Clr",1,"inNormal");
//      shader->linkProgramObject("Clr");


      glGenVertexArrays(1, &m_vao);
      glBindVertexArray(m_vao);

      glGenBuffers(1, &vertexBuf);
      glGenBuffers(1, &normalBuf);


      glBindBuffer(GL_ARRAY_BUFFER, vertexBuf);
      glBufferData(GL_ARRAY_BUFFER, TotalConnectedTriangles*sizeof(GLfloat),&Faces_Triangles[0] , GL_STATIC_DRAW);
      glEnableVertexAttribArray(0);
      glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0); // Set up our vertex attributes pointer


      glBindBuffer(GL_ARRAY_BUFFER, normalBuf);
      glBufferData(GL_ARRAY_BUFFER, TotalConnectedTriangles*sizeof(GLfloat),&Faces_normals[0] , GL_STATIC_DRAW);
      glEnableVertexAttribArray(1);
      glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0); // Set up our vertex attributes pointer


     glBindBuffer(GL_ARRAY_BUFFER, 0);

     glBindVertexArray(0);


}



void Model_OBJ::Draw()
{
//    ngl::VAOPrimitives *prim=ngl::VAOPrimitives::instance();

 /*   ngl::ShaderLib *shader=ngl::ShaderLib::instance();
    shader->use("Phong");*/    // draw
  //  prim->draw("teapot");

//    glEnableVertexAttribArray(0);
//    glBindBuffer(GL_ARRAY_BUFFER, vertexBuf);
//    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

//    glEnableVertexAttribArray(normalLocation);
//    glBindBuffer(GL_ARRAY_BUFFER, normalBuf);
//    glVertexAttribPointer((GLuint)normalLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);

  //  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indBuf);
    glBindVertexArray(m_vao);

    glDrawArrays(GL_TRIANGLES, 0,TotalConnectedTriangles/3);

    glBindVertexArray(0);



}


