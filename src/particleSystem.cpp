/*
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#include "particleSystem.h"
#include "particleSystem.cuh"
#include "particles_kernel.cuh"
#include <cuda_runtime.h>
#include <assert.h>
#include <math.h>
#include "bounding.h"
#include <pthread.h>

//cloth size
const static int SIZE = 20;			// drawing size of grid make it global makes it easy to change the size
const static int  num_thrd1 = 7;		//note: designed to work with less than 9 thread !!!
const static int thread1=  0;
//because of thread programming they need to be global                                          /////////////
float *m_hPos;              // particle positions												/////////////
short *m_faces;																					/////////////
int  num;																						/////////////
float dis;																						/////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
class MyThread : public Thread , public ParticleSystem
{
  public:
    void *run(long n) {
    	intptr_t s = n;   // retrive the slice info

    	   int from = (s *num )/num_thrd1; // note that this 'slicing' works fine

    	   int to = ((s+1) *num )/num_thrd1; // even if SIZE is not divisible by num_thrd

           bool t1=0;
           int h,w,j=0,i=0;

    	   for ( h = -num/2+from; h < to-num/2; h++)
    				for ( w = -num/2; w < num/2; w++)
    				{

                        m_hPos[4*s*num*(num/num_thrd1)+i]   = w*dis;
                        m_hPos[4*s*num*(num/num_thrd1)+i+1] = 6;
                        m_hPos[4*s*num*(num/num_thrd1)+i+2] = h*dis;
                        m_hPos[4*s*num*(num/num_thrd1)+i+3] = 1.0f;

    	                i+=4;

    	                if(w==num/2-1 || h==num/2-1)
    	                    		continue;
                        m_faces[6*(num-1)*s*((num-1)/num_thrd1)+j]  =((h+num/2) * num + (w+num/2));
                        m_faces[6*(num-1)*s*((num-1)/num_thrd1)+j+1]=(((h+num/2) + 1) * num + (w+num/2) + 1);;
                        m_faces[6*(num-1)*s*((num-1)/num_thrd1)+j+2]=((h+num/2) * num + (w+num/2) + 1);
                        m_faces[6*(num-1)*s*((num-1)/num_thrd1)+j+3]=((h+num/2) * num + (w+num/2));
                        m_faces[6*(num-1)*s*((num-1)/num_thrd1)+j+4]=(((h+num/2) + 1) * num + (w+num/2));
                        m_faces[6*(num-1)*s*((num-1)/num_thrd1)+j+5]=(((h+num/2) + 1) * num + (w+num/2) + 1);
    					j+=6;
    					t1=1;
    				}
           //  to check thread and the number that assign to that thread help debugging
         //  printf("thread %lu running - %d\n",  (unsigned int)self(),n);
    	   return NULL;

    }

};


ParticleSystem::ParticleSystem(uint numParticles, uint3 gridSize,float *b_mid,int size,int _cellsize,float * _face,bool _th):
    m_bInitialized(false),
    thread(_th),
    m_numParticles(numParticles),
	textcoords(0),
	norm(0),
	midBound(b_mid),
	objFace(_face),
    m_dPos(0),
    m_dface(0),
    m_gridSize(gridSize),
    m_timer(NULL),
    m_solverIterations(1),
    objSize(size),
    m_numGridCells(m_gridSize.x*m_gridSize.y*m_gridSize.z)

{

    m_params.gridSize=m_gridSize;
    m_params.numCells=m_numGridCells;
    m_params.numBodies=m_numParticles;
    m_params.colliderPos=make_float3(0.0f, 0.0f, 0.0f);
    m_params.worldOrigin = make_float3(-1.0f, -1.0f, -1.0f);
    m_params.cellSize = make_float3(_cellsize*_cellsize,_cellsize*_cellsize,_cellsize*_cellsize);
    m_params.stiffness=112000;			//change cloth stiffness
    m_params.hang=0;					// 0 drop the cloth (1) hang from one side (2) hang from middle (3) hang one point in middle
    m_params.globalDamping = 2.5f;			///  change cloth damping


    m_params.inverse_mass=64/(m_numParticles*m_numParticles);
    m_params.spring = 1.0f;
    m_params.damping = 0.02f;
    m_params.shear = 0.1f;
    m_params.attraction = 0.0f;
    m_params.boundaryDamping = -0.5f;
    m_params.gravity = make_float3(0.0f, -9.8f, 0.0f);

    m_params.ground=0;
    num=(int)sqrt(numParticles);
    dis=SIZE/(double)(num - 1);

    initialize();

}

ParticleSystem::~ParticleSystem()
{
    assert(m_bInitialized);
    delete [] m_hPos;
    delete [] m_hCellStart;
    delete [] m_hCellEnd;
	delete [] m_hOldPos;
	delete [] norm;
    delete [] m_faces;
	delete [] textcoords;

    freeArray(m_dSortedPos);
    freeArray(m_dSortedVel);
	freeArray(m_dface);
    freeArray(m_dGridParticleHash);
    freeArray(m_dGridParticleIndex);
    freeArray(m_dCellStart);
    freeArray(m_dCellEnd);
    unregisterGLBufferObject(m_cuda_posvbo_resource);
	unregisterGLBufferObject(m_cuda_norvbo_resource);

    glDeleteBuffers(1, (const uint *)&m_posVbo);
    glDeleteBuffers(1, (const GLuint *)&m_colorVBO);
	glDeleteBuffers(1, (const GLuint *)&m_normVbo);
    m_numParticles = 0;
}

uint ParticleSystem::createVBO(uint size)
{
    GLuint vbo;

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, size, 0, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return vbo;
}


void ParticleSystem::initialize()
{
    initializeGLFunctions();

    assert(!m_bInitialized);

    // allocate host storage
    m_hPos =	new float[m_numParticles*4+objSize];
    m_hOldPos=	new float[m_numParticles*4];
	norm =		new float[m_numParticles*4];
	textcoords= new float[m_numParticles*2];
	m_faces=    new short[(num-1)*(num-1)*6];
	memset(m_faces, 0 ,(num-1)*(num-1)*6*sizeof(short));
    memset(textcoords, 0, m_numParticles*2*sizeof(float));
	memset(m_hPos, 0, (m_numParticles*4+objSize)*sizeof(float));
	memset(m_hOldPos, 0,  (m_numParticles*4)*sizeof(float));
    memset(norm, 0, m_numParticles*4*sizeof(float));
	m_hCellStart = new uint[m_numGridCells];
    memset(m_hCellStart, 0, m_numGridCells*sizeof(uint));
    m_hCellEnd = new uint[m_numGridCells];
    memset(m_hCellEnd, 0, m_numGridCells*sizeof(uint));
    // allocate GPU data
    unsigned int memSize = sizeof(float) * 4 * m_numParticles;
    m_posVbo = createVBO(memSize+sizeof(float)*objSize);
    m_normVbo= createVBO(memSize);
    registerGLBufferObject(m_posVbo, &m_cuda_posvbo_resource);
	registerGLBufferObject(m_normVbo, &m_cuda_norvbo_resource);
	float boundSize=objSize*sizeof(float);

    allocateArray((void **)&m_dface,boundSize*3);
	copyArrayToDevice(m_dface, objFace,0,boundSize*3);

    allocateArray((void **)&m_dSortedPos, (m_numParticles*4+objSize)*sizeof(float));
    allocateArray((void **)&m_dSortedVel, (m_numParticles*4+objSize)*sizeof(float));
    allocateArray((void **)&m_dGridParticleHash, (m_numParticles+objSize/4)*sizeof(uint));
    allocateArray((void **)&m_dGridParticleIndex, (m_numParticles+objSize/4)*sizeof(uint));
    allocateArray((void **)&m_dCellStart, m_numGridCells*sizeof(uint));
    allocateArray((void **)&m_dCellEnd, m_numGridCells*sizeof(uint));
    sdkCreateTimer(&m_timer);
    setParameters(&m_params);
	InCuda(m_numParticles*4+objSize);
    m_bInitialized = true;
}

// step the simulation
void ParticleSystem::update()
{
    assert(m_bInitialized);
	// update constants
    setParameters(&m_params);
    float *dPos , *nor;
    dPos = (float *) mapGLBufferObject(&m_cuda_posvbo_resource);
	nor =(float *)mapGLBufferObject(&m_cuda_norvbo_resource);
	UploadCuda(&(m_hPos[0]),&(m_hOldPos[0]),m_numParticles+objSize/4);
    calcHash(
        m_dGridParticleHash,
        m_dGridParticleIndex,
        m_numParticles,objSize/4);

    // sort particles based on hash
	sortParticles(m_dGridParticleHash, m_dGridParticleIndex, m_numParticles+objSize/4);

    // reorder particle arrays into sorted order and
    // find start and end of each cell
    reorderDataAndFindCellStart(
        m_dCellStart,
        m_dCellEnd,
        m_dSortedPos,
        m_dGridParticleHash,
        m_dGridParticleIndex,
        m_numParticles,
        objSize/4,
        m_numGridCells);

    VerletCuda(dPos,nor,m_numParticles, texture_size,

		m_dSortedPos,
        m_dGridParticleIndex,
        m_dCellStart,
        m_dCellEnd,
        m_numGridCells,
		objSize/4,
		m_dface);

    unmapGLBufferObject(m_cuda_posvbo_resource);
	unmapGLBufferObject(m_cuda_norvbo_resource);
}


void ParticleSystem::BuildCloth(uint numParticles)
{
  // ResetCuda();
#if(!thread1)
	uint i =0,j=0;;
	for (int h = -num/2; h < num/2; h++)
			for (int w = -num/2; w < num/2; w++)
			{
                    m_hPos[i]   = w*dis;
                    m_hPos[i+1] = 6;
                    m_hPos[i+2] = h*dis;
                    m_hPos[i+3] = 1.0f;
                    i+=4;
                if(w==num/2-1 || h==num/2-1)
                    		continue;
				m_faces[j]  =((h+num/2) * num + (w+num/2));
				m_faces[j+1]=(((h+num/2) + 1) * num + (w+num/2) + 1);;
				m_faces[j+2]=((h+num/2) * num + (w+num/2) + 1);
				m_faces[j+3]=((h+num/2) * num + (w+num/2));
				m_faces[j+4]=(((h+num/2) + 1) * num + (w+num/2));
				m_faces[j+5]=(((h+num/2) + 1) * num + (w+num/2) + 1);
				j+=6;
			}
#else
MyThread* t_array = static_cast<MyThread*>( ::operator new ( sizeof (MyThread) * num_thrd1 ) );
for ( size_t i = 0; i < num_thrd1; i++ )
{
  new (&t_array[i]) MyThread();
  t_array[i].start(i);
}
	//	MyThread* thread0 = new MyThread();
	for (int i = 0; i < num_thrd1; i++)
		{
		t_array[i].join();
		}

#endif
	 m_face=m_faces;
	 m_params.step=dis;
     memcpy(&m_hPos[0]+(numParticles*4),&midBound[0],objSize*sizeof(float));

     InitBuffers(numParticles);
}
void ParticleSystem::InitBuffers(int num)
{
		texture_size = nearest_pow(ceil(sqrt((float)num)));
		m_hOldPos=m_hPos;
		int j=0;
				for (int i = 0; i < num; i++)
				{
					textcoords[j]=(m_hPos[i * 4]);
					textcoords[j+1]=(m_hPos[i * 4 + 2]);
					j+=2;
				}
}
void ParticleSystem::reset()
{
 BuildCloth(m_numParticles);
}
void ParticleSystem::setnorm()
{
	for (int i = 0; i < 4; i++)
		norm[(m_numParticles - 1) * 4 + i] = norm[(m_numParticles - 2) * 4 + i];
}
