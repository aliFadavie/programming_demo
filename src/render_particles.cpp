#include "render_particles.h"


ParticleRenderer::ParticleRenderer()
    : m_pos(0),
      m_numParticles(0),
      m_pointSize(1.0f),
      m_particleRadius(0.125f * 0.5f),
      m_program(0),
	  m_normVbo(0),
      faces(0),
      texcoords(0),
      m_vbo(0),
      m_vao(0),
      isWireframe(0),
      renderShaderProgram(-1),
      m_tex(0)
{}

ParticleRenderer::~ParticleRenderer()
{
    //m_pos = 0;
    m_numParticles=0;

}

void ParticleRenderer::setPositions( float *pos, int numParticles)
{
    m_pos = pos;
    m_numParticles = numParticles;
}

void ParticleRenderer::setVertexBuffer(unsigned int posvbo,unsigned int normvbo, float* tex , short* _face,int numParticles,bool _wire)
{
	faces=_face;
    texcoords=tex;
	m_vbo = posvbo;
	m_normVbo=normvbo;
    m_numParticles = numParticles;
    isWireframe=_wire;
}

void ParticleRenderer::DrawParticleSystem()
{
   int sq=sqrt((float)m_numParticles);

   glGenVertexArrays(1, &m_vao);
   glBindVertexArray(m_vao);

   glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
   glEnableVertexAttribArray(0);
   glVertexAttribPointer((GLuint)0, 4, GL_FLOAT, GL_FALSE, 0, 0); // Set up our vertex attributes pointer

   glGenBuffers(1, &m_tex);
   glBindBuffer(GL_ARRAY_BUFFER, m_tex);
   glBufferData(GL_ARRAY_BUFFER, m_numParticles*2*sizeof(GLfloat),texcoords , GL_STATIC_DRAW);
   glEnableVertexAttribArray(2);
   glVertexAttribPointer((GLuint)2,2 , GL_FLOAT, GL_FALSE, 0, 0);
   glBindBuffer(GL_ARRAY_BUFFER, m_normVbo);
   glEnableVertexAttribArray(1);
   glVertexAttribPointer((GLuint)1, 4, GL_FLOAT, GL_FALSE, 0, 0);

   glBindBuffer(GL_ARRAY_BUFFER, 0);

   glBindVertexArray(0);

   glBindVertexArray(m_vao);

   glDrawElements(GL_TRIANGLES, (GLsizei)((sq-1)*(sq-1)*6), GL_UNSIGNED_SHORT, &faces[0]);

   glBindVertexArray(0);

   glDisableVertexAttribArray(0);
   glDisableVertexAttribArray(1);
   glDisableVertexAttribArray(2);


}











