/////////////The Fabric shader is taken from RenderMonkey  1.82 by AMD and 3Dlabs
//it is a very good shader how ever the implementation is out of date and deprecated so I just rewrite it again.
#version 400 core
layout (location = 0) in vec3 inVert;
/// @brief the normal passed in
layout (location = 1) in vec3 inNormal;

/// @brief the in uv
layout (location = 2) in vec2 inUV;

uniform mat4 MV;
uniform mat4 MVP;
uniform mat3 normalMat;
uniform mat4 M;
out vec3 vPosition;
out vec3 vNormal;
out vec3 vViewVec;


void main(void)
{
	float scale = 1.0;

        gl_Position =   MVP*vec4(inVert,1);
   
	// Pass position to fragment shader
        vPosition = vec3(inUV * scale,1);
      //   vPosition=MV*vec4(inVert,1);
	// Eye-space lighting

        vNormal = vec3(normalMat * inNormal);
        //vNormal = gl_NormalMatrix * gl_Normal;
   
        vViewVec   = -vec3(MV *vec4(inVert,1));
}
