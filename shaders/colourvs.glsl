#version 400

//// test shader
layout (location = 0) in vec3 inVert;
//layout (location = 1) in vec3 inNormal;

uniform mat4 MVP;


void main(void)
{
     gl_Position = MVP*vec4(inVert, 1.0);
}
