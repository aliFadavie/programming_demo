#version 400

//test shader
uniform vec4 Colour;

layout (location =0) out vec4 fragColour;

void main ()
{
  fragColour = Colour;
}
