#ifndef __RENDER_PARTICLES__
#define __RENDER_PARTICLES__


#include <ngl/VAOPrimitives.h>
#include "glslprogram.h"
#include <math.h>
#include <stdio.h>
#include <iostream>

#include <QGLFunctions>
class ParticleRenderer: public QGLFunctions
{
    public:
         ParticleRenderer();
        ~ParticleRenderer();

        void setPositions( float *pos, int numParticles);
        void setVertexBuffer(unsigned int vbo,unsigned int norm_vbo,float* tex,short* _face, int numParticles,bool _wire);

        void displayGrid();

        void setPointSize(const float size)
        {
            m_pointSize = size;
        }
        void setParticleRadius(const float r)
        {
            m_particleRadius = r;
        }
        void setFOV(const float fov)
        {
            m_fov = fov;
        }
        void setWindowSize(const int w,const int h)
        {
            m_window_w = w;
            m_window_h = h;
        }
        void DrawParticleSystem();

protected: // data
        float *m_pos;
        int m_numParticles;

        float m_pointSize;
        float m_particleRadius;
        float m_fov;
        int m_window_w, m_window_h;

        GLuint m_program;
        bool isWireframe;
        GLuint m_vbo;
        GLuint m_normVbo;
        GLuint m_vao;
        float *texcoords;
		short *faces;
		GLuint renderShaderProgram;
        GLuint m_tex;

};

#endif //__ RENDER_PARTICLES__
