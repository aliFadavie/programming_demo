/*
 * thread.h
 *
 *  Created on: Oct 20, 2013
 *      Author: i7220258
 */
//Java Style Thread Class in C++
//http://vichargrave.com/java-style-thread-class-in-c/
#ifndef THREAD_H_
#define THREAD_H_

#include <pthread.h>

class Thread
{
  public:
    Thread();
    virtual ~Thread();

    int start(int n);
    int join();
    int detach();

    pthread_t self();

    virtual void* run(long n) = 0;

  private:
    pthread_t  m_tid;
    int        m_running;
    int        m_detached;
    int 	   m;

};
#endif /* THREAD_H_ */
