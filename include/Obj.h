/*
 *
 * Demonstrates how to load and display an Wavefront OBJ file. 
 * Using triangles and normals as static object. No texture mapping.
 *
 * OBJ files must be triangulated!!!
 * Non triangulated objects wont work!
 * You can use Blender to triangulate
 *
 */

#ifndef __OBJ_H__
#define __OBJ_H__

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#include <ngl/Camera.h>
#include <ngl/Types.h>
//#include "glslprogram.h"
#include <QGLFunctions>
#include <ngl/ShaderLib.h>
#include<ngl/VAOPrimitives.h>
#include <ngl/Light.h>

class Model_OBJ : protected QGLFunctions
{
  public: 
    Model_OBJ();
    ~Model_OBJ();
    int   Load(char *filename);	// Loads the model
    void  Draw();				// Draws the model on the screen
    int   getTotalTriangle()const     {return TotalConnectedTriangles;}
    float getFacesArr(int x)const     {return Faces_Triangles[x] ;}
    int   getTotalPoint()   const	  {return TotalConnectedPoints;}
    void initBuf();
  private:
    std::vector<float> normals;							// Stores the normals
    std::vector<float> Faces_Triangles;					// Stores the triangles
    std::vector<float> Faces_normals;
    std::vector<float> vertexBuffer;					// Stores the points which make the object
    std::vector<unsigned short> indexBuf;
    int TotalConnectedPoints;				// Stores the total number of connected verteces
	int TotalConnectedTriangles;			// Stores the total number of connected triangles
    GLuint vertexBuf;
    GLuint normalBuf;
    GLuint indBuf;
    GLuint m_vao;
//	GLSLProgram tem;
};

#endif
