#include <stdlib.h>
#include <vector_types.h>
#include <vector_functions.h>
#include <limits>
#include "helper_math.h"



class bounding
{
private:
	float3 bounding_box[2];
    float3 mid_box;
public:
    float4 set_bb(float * verts)
    {
        bounding_box[0]=make_float3(100000.0f);
        bounding_box[1]=make_float3(0.0f);
        int j=0;
        for(unsigned int i = 0; i < 3; i++)
        {
            //update min value
            bounding_box[0].x = (verts[j]  <bounding_box[0].x)? verts[j]:bounding_box[0].x;
            bounding_box[0].y = (verts[j+1]<bounding_box[0].y)? verts[j+1]:bounding_box[0].y;
            bounding_box[0].z = (verts[j+2]<bounding_box[0].z)? verts[j+2]:bounding_box[0].z;

            //update max value
            bounding_box[1].x = (verts[j]  >bounding_box[1].x)? verts[j]:bounding_box[0].x;
            bounding_box[1].y = (verts[j+1]>bounding_box[1].y)? verts[j+1]:bounding_box[0].y;
            bounding_box[1].z = (verts[j+2]>bounding_box[1].z)? verts[j+2]:bounding_box[0].z;

            j+=3;

        }
        mid_box=(bounding_box[0]+bounding_box[1])/2;
        return make_float4(mid_box,length(mid_box));
	}
};
