#ifndef __PARTICLESYSTEM_H__
#define __PARTICLESYSTEM_H__

#include <helper_functions.h>
#include "particles_kernel.cuh"
#include "vector_functions.h"
#include "thread.h"
#include <QGLFunctions>
// Particle system class
class ParticleSystem : public QGLFunctions
{
    public:
        ParticleSystem(uint numParticles, uint3 gridSize, float * b_mid,int size,int _cellsize,float *face,bool _th);
        ~ParticleSystem();

        void update();
        void reset();

        int    getNumParticles() const
        {
            return m_numParticles;
        }
		 short* getCurrentFaceBuffer() const
        {
            return m_face;
        }

        unsigned int getCurrentReadBuffer() const
        {
            return m_posVbo;
        }

		  float* getCurrentcoordBuffer() const
        {
            return textcoords;
        }
		 unsigned int getCurrentNormBuffer() const
        {
            return m_normVbo;
        }
		 void setnorm();
         void setColliderPos(const float3 x)
         {
            m_params.colliderPos = x;
         }
         float3 getColliderPos() const
         {
            return m_params.colliderPos;
         }

		 /////////////////////////////////////////////////
        unsigned int getColorBuffer()const
        {
            return m_colorVBO;
        }

        void *getCudaPosVBO()const
        {
            return (void *)m_cudaPosVBO;
        }
        void *getCudaColorVBO()const
        {
            return (void *)m_cudaColorVBO;
        }


        void setIterations(int i)
        {
            m_solverIterations = i;
        }

        void setDamping(const float x)
        {
            m_params.globalDamping = x;
        }
        void setGravity(const float x)
        {
            m_params.gravity = make_float3(0.0f, x, 0.0f);
        }

        void setCollideSpring(const float x)
        {
            m_params.spring = x;
        }
        void setCollideDamping(const float x)
        {
            m_params.damping = x;
        }
        void setCollideShear(const float x)
        {
            m_params.shear = x;
        }
        void setCollideAttraction(const float x)
        {
            m_params.attraction = x;
        }
        uint3 getGridSize() const
        {
            return m_params.gridSize;
        }
        float3 getWorldOrigin()const
        {
            return m_params.worldOrigin;
        }
        float3 getCellSize()const
        {
            return m_params.cellSize;
        }

protected: // methods

    ParticleSystem() {}
    uint createVBO(uint size);

    void initialize();
	static unsigned int nearest_pow (unsigned int num)
	{
		unsigned int n = num > 0 ? num - 1 : 0;

		n |= n >> 1;
		n |= n >> 2;
		n |= n >> 4;
		n |= n >> 8;
		n |= n >> 16;
		n++;

		return n;
	}

    void BuildCloth(uint numParticles);
    void InitBuffers(int num);

private: // data
        bool m_bInitialized, m_bUseOpenGL;
        uint m_numParticles;

        // CPU data

		float *m_hOldPos;
		float *norm;

		float *textcoords;
		uint  *m_hParticleHash;
        uint  *m_hCellStart;
        uint  *m_hCellEnd;

		float *objFace;

		float *midBound;
		float *h_mix;
		float *d_mix;
		uint objSize;
		int texture_size;
        // GPU data
        float *m_dPos;

		float *m_dface;

        float *m_dSortedPos;
        float *m_dSortedVel;

        // grid data for sorting method

        uint  *m_dGridParticleHash; // grid hash value for each particle
        uint  *m_dGridParticleIndex;// particle index for each particle
        uint  *m_dCellStart;        // index of start of each cell in sorted list
        uint  *m_dCellEnd;          // index of end of cell
        uint   m_posVbo;            // vertex buffer object for particle positions
        uint   m_normVbo;
		uint   m_colorVBO;          // vertex buffer object for colors

        float *m_cudaPosVBO;        // these are the CUDA deviceMem Pos
        float *m_cudaColorVBO;      // these are the CUDA deviceMem Color

        struct cudaGraphicsResource *m_cuda_posvbo_resource;   // handles OpenGL-CUDA exchange
        struct cudaGraphicsResource *m_cuda_norvbo_resource;  // calculating normals
		struct cudaGraphicsResource *m_cuda_colorvbo_resource; // handles OpenGL-CUDA exchange

        // params
        SimParams m_params;
        uint3 m_gridSize;
        uint m_numGridCells;

        StopWatchInterface *m_timer;

        uint m_solverIterations;
        short* m_face;
        bool thread;


};

#endif // __PARTICLESYSTEM_H__
