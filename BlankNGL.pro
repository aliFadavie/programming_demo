TARGET    = QtCuda
QT+=gui
QT+=opengl
QT+= core
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE  = app
OBJECTS_DIR=obj
# as I want to support 4.8 and 5 this will set a flag for some of the mac stuff
# mainly in the types.h file for the setMacVisual which is native in Qt5
isEqual(QT_MAJOR_VERSION, 5) {
		cache()
		DEFINES +=QT5BUILD
}

# project build directories
DESTDIR     = $$system(pwd)
OBJECTS_DIR = $$DESTDIR/Obj
# C++ flags
QMAKE_CXXFLAGS_RELEASE =-O3

MOC_DIR=moc
CONFIG-=app_bundle


SOURCES += \
    src/thread.cpp \
    src/render_particles.cpp \
    src/particleSystem.cpp \
    src/particles.cpp \
    src/OpenGLWindow.cpp \
    src/Obj.cpp \
    src/NGLScene.cpp \
    src/main.cpp \
    src/glslprogram.cpp \
    kernel/particleSystem_cuda.cu


SOURCES -= kernel/particleSystem_cuda.cu

HEADERS += \
    include/thread.h \
    include/render_particles.h \
    include/particleSystem.h \
    include/OpenGLWindow.h \
    include/Obj.h \
    include/NGLScene.h \
    include/Miniball.hpp \
    include/glslprogram.h \
    include/bounding.h \
    kernel/particles_kernel.cuh \
    kernel/particles_kernel_impl.cuh\
    kernel/particleSystem.cuh

INCLUDEPATH +=./include
INCLUDEPATH +=./kernel



OTHER_FILES+= \
    shaders/fabric_plaidfs.glsl \
    shaders/fabric_plaidvs.glsl \
    shaders/colourvs.glsl \
    shaders/colourfs.glsl \
    shaders/PhongVertex.glsl \
    shaders/PhongFragment.glsl




# use this to suppress some warning from boost
unix:QMAKE_CXXFLAGS_WARN_ON += "-Wno-unused-parameter"

unix:QMAKE_CXXFLAGS+= -msse -msse2 -msse3
macx:QMAKE_CXXFLAGS+= -arch x86_64
macx:INCLUDEPATH+=/usr/local/include/
linux-g++:QMAKE_CXXFLAGS +=  -march=native
linux-g++-64:QMAKE_CXXFLAGS +=  -march=native
# define the _DEBUG flag for the graphics lib
DEFINES +=NGL_DEBUG

unix:LIBS += -L/usr/local/lib
# add the ngl lib
unix:LIBS +=  -L/$(HOME)/NGL/lib -l NGL

# now if we are under unix and not on a Mac (i.e. linux) define GLEW
linux-*{
		linux-*:QMAKE_CXXFLAGS +=  -march=native
		linux-*:DEFINES+=GL42
		DEFINES += LINUX
}
DEPENDPATH+=include

# if we are on a mac define DARWIN
macx:DEFINES += DARWIN
# this is where to look for includes
INCLUDEPATH += $$(HOME)/NGL/include/

win32: {
				PRE_TARGETDEPS+=C:/NGL/lib/NGL.lib
				INCLUDEPATH+=-I c:/boost
				DEFINES+=GL42
				DEFINES += WIN32
				DEFINES+=_WIN32
				DEFINES+=_USE_MATH_DEFINES
				LIBS += -LC:/NGL/lib/ -lNGL
				DEFINES+=NO_DLL
}



# Cuda sources
CUDA_SOURCES += kernel/particleSystem_cuda.cu

# Path to cuda toolkit install
CUDA_DIR      = /usr/local/cuda-5.5
# Path to header and libs files
INCLUDEPATH  += $$CUDA_DIR/include
INCLUDEPATH  += $$CUDA_DIR/samples/common/inc
QMAKE_LIBDIR += $$CUDA_DIR/lib64     # Note I'm using a 64 bits Operating system
QMAKE_LIBDIR += $$CUDA_DIR/samples/common/lib/linux/x86_64
# libs used in your code
LIBS += -lcudart -lcuda

#disable member apear in the initialzerlist warning, with out this you will get unnecessary warning
QMAKE_CXXFLAGS_WARN_ON += -Wno-reorder

QMAKE_CXXFLAGS += -DGLEW_STATIC
# GPU architecture
CUDA_ARCH     = sm_30                # Yeah! I've a new device. Adjust with your compute capability
# Here are some NVCC flags I've always used by default.
NVCCFLAGS     = --compiler-options -fno-strict-aliasing -use_fast_math --ptxas-options=-v

# Prepare the extra compiler configuration (taken from the nvidia forum - i'm not an expert in this part)
CUDA_INC = $$join(INCLUDEPATH,' -I','-I',' ')

cuda.commands = $$CUDA_DIR/bin/nvcc -m64 -O3 -arch=$$CUDA_ARCH -c $$NVCCFLAGS \
                $$CUDA_INC $$LIBS  ${QMAKE_FILE_NAME} -o ${QMAKE_FILE_OUT} \
# nvcc error printout format ever so slightly different from gcc
# http://forums.nvidia.com/index.php?showtopic=171651
                2>&1 | sed -r \"s/\\(([0-9]+)\\)/:\\1/g\" 1>&2

cuda.dependency_type = TYPE_C # there was a typo here. Thanks workmate!
cuda.depend_command = $$CUDA_DIR/bin/nvcc -O3 -M $$CUDA_INC $$NVCCFLAGS   ${QMAKE_FILE_NAME}

cuda.input = CUDA_SOURCES
cuda.output = ${OBJECTS_DIR}${QMAKE_FILE_BASE}_cuda.o
# Tell Qt that we want add more stuff to the Makefile
QMAKE_EXTRA_COMPILERS += cuda




